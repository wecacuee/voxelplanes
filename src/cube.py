import numpy as np
import matplotlib.mlab as mlab

from tvtk.api import tvtk

import visualiser
import utils

class Cube:
    verts = np.array(
    [[0, 0, 0],
    [0, 0, 1],
    [0, 1, 0],
    [0, 1, 1],
    [1, 0, 0],
    [1, 0, 1],
    [1, 1, 0],
    [1, 1, 1]])

    edges = np.array([
    (0, 4),
    (4, 6),
    (6, 2),
    (2, 0),
    (0, 1),
    (4, 5),
    (6, 7),
    (2, 3),
    (1, 5),
    (5, 7),
    (7, 3),
    (3, 1),
    ])


# TODO refactor to use plane objects?
def intersect_plane_cube(normal, p0, cube_size, cube_center):
    # TODO add position and size of cube
    ''' plane has normal and a point p0, cube is axis aligned with position '''

    ''' For a line defined by p1, p2 and plane with normal n and point p0
    intersection is 
    u = n . (p0 - p1)
        -------------
        n . (p2 - p1)
    '''

    # TODO problem if normal is 0, 0, 0
    N = normal
    verts = Cube.verts * cube_size + cube_center - cube_size/2
    p1s = verts[Cube.edges[:, 0]] 
    p2s = verts[Cube.edges[:, 1]] 
    p1, p2 = verts[Cube.edges[0]]

    denom = np.dot(p2s - p1s, N)
    numer = np.dot(p0 - p1s, N)
    # denom is zero if the line edge is parallel to the plane and so will not intersect it

    NZ = denom != 0 # non zero inds
    # remove all elements corresponding to zero denom fixes division by zero warnings
    
    # denom is zero also if the line lies on the plane. In that case the
    # numerator is also zero
    OVERLAP = ((denom == 0) & (numer == 0))
    p1overlap = p1s[OVERLAP]
    p2overlap = p2s[OVERLAP]

    if not np.all(denom): 
        denom = denom[NZ]
        numer = numer[NZ]
        p1s = p1s[NZ]
        p2s = p2s[NZ]

    U = numer / denom
    U.shape = -1, 1 # make U column vector
    Ps = p1s + U*(p2s - p1s)
    #U = np.dot(p0 - p1, N)/np.dot(p2 - p1, N)
    # position of intersection is P = p1 + u(p2 - p1)
    # so for intersect with cube edge U is between 0 and 1
    inds = np.squeeze((U > 0) & (U < 1))
    return np.vstack((Ps[inds], p1overlap, p2overlap))

def triangulate(points):
    ''' Fills a set of convex planar points with triangles returning the
    points (in different order) and triangle indices '''

    # calculate point order 
    # The points all lie on plane so just need to consider the line joining a
    # random pair of points then order points by angle to this line as
    # calculated by dot product.

    # TODO reordering is only necessary for 5 or 6 points
    #if len(points) > 4:
    #P = points[np.lexsort(points.T)] # sort vertices by each axis this does not seem to work

    # pick random axis through polygon by selecting two points and then order
    # points by how far along this axis they are via the dot product which
    # gives the projection of line
    P = points
    ref_line = P[1] - P[0]
    #projections = np.dot(P - P[0], ref_line)
    # re-order P according to projections
    #P = P[np.argsort(projections)]

    ref_line = P[1] - P[0]
    ref_line /= np.linalg.norm(ref_line)
    X = P[1:] - P[0]
    cos_thetas = np.dot(X, ref_line)/mlab.vector_lengths(X, axis=1)

    # sometimes cos(theta) is 1.00000002 and so inverse cos is invalid
    # enforce -1 to 1 range on cos_theta
    cos_thetas = np.clip(cos_thetas, -1, 1)

    thetas = np.arccos(cos_thetas)
    thetas = np.hstack((0, thetas))

    P = P[np.argsort(thetas)]

    # TODO this inds definition does not need to be done every time
    #inds = np.array([
        #(0, 1, 2), 
        #(1, 2, 3), 
        #(2, 3, 4), 
        #(3, 4, 5), 
        #])
    inds = np.array([
        (0, 1, 2), 
        (0, 2, 3), 
        (0, 3, 4), 
        (0, 4, 5), 
       ])
    inds = inds[:len(P) - 2] # only need 4 triangles to cover 6 points

    return P, inds

#n = (1, 1, 1)
#p0 = (0.5, 0.5, 0.5)
#p0 = (0.7, 0.7, 0.7)
# TODO should use vtk Polygon but cannot get it to work under tvtk

def show_bounded_planes(positions, normals, cube_centers, cube_size,
                         rgbs=None, output_file=None, vis=True):
    ''' Show the plane elements defined by positions and normals bounded by
    voxel of size resolution ''' 
    points = []
    triangles = []
    point_rgbs = []

    if rgbs is None: # colour by normal?
        rgbs = utils.rgbs_by_normals(normals)

    assert len(rgbs) == len(positions) # TODO need to add the other asserts?

    for p0, n, cube_center, rgb in zip(positions, normals, cube_centers, rgbs):
    #for p0, n, cube_center in zip(positions, normals, cube_centers):
        P = intersect_plane_cube(n, p0, cube_size, cube_center)
        if len(P) < 3: continue
        P, inds = triangulate(P)
        triangles.extend(inds + len(points))
        points.extend(P)
        # replicate color for each of the triangles in this voxel
        point_rgbs.extend(np.tile(rgb, (len(inds), 1))) 

    triangles = np.array(triangles)
    points = np.array(points)
    point_rgbs = np.array(point_rgbs, dtype=np.uint8)
    polydata = tvtk.PolyData(points=points, polys=triangles)

    # Add point colors to vtk mesh
    polydata.point_data.scalars = None
    #polydata.point_data.scalars = rgbs
    #polydata.point_data.scalars.name = 'scalars'
    polydata.verts, polydata.lines = None, None
    polydata.cell_data.scalars = point_rgbs
    polydata.cell_data.scalars.name = 'colors'


    if output_file:
        # write the visualization to output file.
        # the file can be visualized by pcd_viewer
        # may be mayavi2 (untested)
        pdw = tvtk.PolyDataWriter(file_name=output_file, input=polydata)
        pdw.write()

    if vis:
        visualiser.visualise(polydata, color_by_normals=(rgbs==None))

if __name__ == '__main__':
    count = 5
    positions = np.random.random((count, 3)) * 2
    normals = np.random.random((count, 3))
    #normals = np.array([(1, 1, 1), ])
    #positions = np.array([(0.4, 0.4, 0.4), ])

    visualiser.clear_window()
    show_bounded_planes(positions, normals, 0.2)
    visualiser.show_window()

#for i in range(10):
#    n = np.random.random(3)
#    p0 = np.random.random(3)
#
#    print 'Normal, point:', n, p0
#
#    P = intersect_plane_cube(n, p0)
#    P, inds = triangulate(P)
#    polydata = tvtk.PolyData(points=P, polys=inds)
#
#    visualiser.clear_window()
#    visualiser.visualise(polydata)
#    visualiser.show_window()
