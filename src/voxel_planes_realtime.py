import opennicapture as ocap
import cube
import voxel_planes 
import voxelcloud
import visualiser
from tvtk.api import tvtk

### Constants 
res = 0.05
R = 5
# visualise in a different process
visualiser.IPC_VIS = True


### main method ###

#initialize
cap = ocap.OpenniCapDevShm()
cap.init()
cap.grab()
#loop
def timer_callback():
    cap.grab()
    vc = voxelcloud.Voxelcloud((-R, -R, -R), (R, R, R), resolution=res)
    vc.addpoints(cap.points())

    vertices, EV = voxel_planes.planes3(vc)
    normals = EV['evecs'][:, voxel_planes.min_eig_ind, :] # minimum is last one
    cube.show_bounded_planes(EV['centroid'], normals, vertices, res,
                              output_file='snapshot.out', vis=True)
    #visualiser.clear_window()
    #vtkRenWinInt.GetRenderWindow().Render()

for i in xrange(1000):
    timer_callback()

