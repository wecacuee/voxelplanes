import numpy as np

def npdtypefrommetadata(metadata):
    formats = ['%s%d' % (t.lower(), s) 
               for t, s in zip(metadata['TYPE'], metadata['SIZE'])]
    return np.dtype({'names': metadata['FIELDS'],
                     'formats' : formats})

def readpcd(filehandle):
    if not hasattr(filehandle, 'readlines'):
        raise ValueError(
            "readpcd(filehandle) accepts a filehandle not type {0}".format(
                type(filehandle)))

    restofdata = list()
    metadata = dict()
    for line in filehandle:
        if "DATA" in metadata:
            restofdata.append(line)
            continue

        line = line.strip()
        if line.startswith("#"):
            # TODO check PCD version
            continue
        words = line.split()
        if words[0] in ("FIELDS", "TYPE"):
            metadata[words[0]] = words[1:]
        elif words[0] in ("DATA", "VERSION"):
            metadata[words[0]] = words[1]
        elif words[0] in ("SIZE", "COUNT"):
            metadata[words[0]] = [int(s) for s in words[1:]]
        elif words[0] in ("WIDTH", "HEIGHT", "POINTS"):
            metadata[words[0]] = int(words[1])
        elif words[0] == "VIEWPOINT":
            metadata[words[0]] = [float(s) for s in words[1:]]
    assert "DATA" in metadata, "Invalid file format"

    npdtype = npdtypefrommetadata(metadata)
    if metadata["DATA"] == "ascii":
        arraydata = list()
        for line in restofdata:
            arraydata.append(tuple(line.strip().split()))
        nparray = np.array(arraydata, npdtype).reshape(metadata['HEIGHT'],
                                                       metadata['WIDTH'])
    elif "binary" in metadata["DATA"]:
        if "binary_compressed" in metadata["DATA"]:
            import lzf
            restofdata = lzf.decompress(restofdata, 100 * len(restofdata))
        nparray = np.fromstring("".join(restofdata),dtype=npdtype)
        nparray.dtype = npdtype
    else:
        assert False, "Invalid format {0}".format(metadata["DATA"])
    return nparray

def unpackrgb(rgbfloat32):
    assert rgbfloat32.dtype == np.float32, "dtype {0}".format(rgbfloat32.dtype)
    oldshape = list(rgbfloat32.shape)
    oldshape.append(4)
    rgbflatten = rgbfloat32.flatten()
    rgbint = np.frombuffer(rgbfloat32.tostring(),
                           dtype=np.uint8).reshape(-1, 4)
    rgbint = rgbint[:, ::-1]
    rgbint = rgbint[:, 1:]
    return rgbint

def pcdtoPointCloud(filehandle):
    pcddata = readpcd(filehandle)
    pcdflatten = pcddata.flatten()
    if 'rgb' in pcdflatten:
        rgb = unpackrgb(pcdflatten['rgb'])

    xyzs = np.vstack((pcdflatten['x'], pcdflatten['y'], pcdflatten['z']))
    if 'rgb' in pcdflatten:
        xyzs = np.hstack((xyzs.T, rgb))
    return xyzs.T

def readpset(filehandle):
    # http://grail.cs.washington.edu/software/pmvs/documentation.html
    # S.pset is simply a list of the 3D locations and estimated surface
    # normals for all the reconstructed points, which can be used as an input
    # to surface reconstruction software (PoissonRecon).
    arraydata = list()
    for line in filehandle:
        cells = line.strip().split()
        arraydata.append(cells)

    return np.array(arraydata, np.float32)

def writepcd(filehandle, xyzs, fields="xyz", width=None, height=None,
             viewpoint=[0, 0, 0, 1, 0, 0, 0], data="binary"):
    if not hasattr(filehandle, 'write'):
        raise ValueError(
            "writepcd(filehandle) accepts a filehandle not type {0}".format(
                type(filehandle)))

    width = xyzs.shape[0]
    height = xyzs.shape[1] if len(xyzs.shape) == 3 else 1
    points = width * height

    assert len(fields) == xyzs.shape[-1]

    # fields
    fields_str = " ".join([f for f in fields])

    # Size must be same for all fields
    size_1 = xyzs.dtype.itemsize
    assert np.all(size_1 <= 4), "Size greater than 4 is unsupported"
    size_str = " ".join([str(size_1) for f in fields])

    # type
    type_1 = xyzs.dtype.kind.upper()
    type_str = " ".join([type_1 for f in fields])

    # count
    cound_str = " ".join(["1" for f in fields])

    viewpoint_str = " ".join(map(str, viewpoint))

    assert (data == "binary") or (data == "ascii")
    filehandle.write(
"""# .PCD v.7 - Point Cloud Data file format
VERSION .7
FIELDS {fields}
SIZE {size}
TYPE {type}
COUNT {count}
WIDTH {width}
HEIGHT {height}
VIEWPOINT {viewpoint}
POINTS {points}
DATA {data}
""".format(fields=fields_str,
           size=size_str,
           type=type_str,
           count=cound_str,
           width=width,
           height=height,
           viewpoint=viewpoint_str,
           points=points,
           fieldsnpoints=xyzs.shape[0],
           data=data))
    if data == "binary":
        filehandle.write(xyzs.tostring())
    elif data == "ascii":
        for xyz in xyzs:
            filehandle.write("%s\n" % " ".join(["%.12f" % f for f in xyz]))
    
