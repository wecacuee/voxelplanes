"""Reads voxelcloud.pickle from /dev/shm/ meshifies and visualizes it"""
import sys
import time
import visualiser
import voxel_planes
import devshmio
from voxelcloud import Voxelcloud
from utils import log

# visualise in a different process
visualiser.IPC_VIS = True

def main(key="voxelcloud"):
    while True:
        try:
            log("Waiting for %s" % key)
            voxelcloud = devshmio.loadfromdevshm(Voxelcloud, key)
        except (EOFError, IOError), e:
            log("Going to retry after error: %s" % str(e))
            # we will retry in some time
            time.sleep(1)
            continue
        voxel_planes.planes3(voxelcloud)

if __name__ == '__main__':
    if len(sys.argv) >= 2:
        main(sys.argv[1])
    else:
        main()
