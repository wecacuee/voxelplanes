import numpy as np
import urllib2
import tarfile
import os
import sys

def get_rgbd_freiburg_dataset(parentdir="/tmp"):
    data_url = "http://vision.in.tum.de/rgbd/dataset/freiburg1/rgbd_dataset_freiburg1_360.tgz"
    fname = "rgbd_dataset_freiburg1_360.tgz"
    localfname = os.path.join(parentdir, fname)
    if not os.path.exists(localfname) or os.stat(localfname)[6] == 0:
        url = urllib2.urlopen(data_url)
        localFile = open(localfname, 'w')
        print 'Downloading test data:', url.geturl(), ' to ', tmp + fname
        localFile.write(url.read())
        localFile.close()
    
    # extract tar ball
    tar = tarfile.open(localfname, mode='r:gz')
    tar.extractall(path=parentdir)
    tar.close()
    return os.path.splitext(localfname)[0]


data_url = 'http://www.cse.buffalo.edu/~jryde/share/' 
tmp = '/tmp/'
# upload data to sol.cse.buffalo.edu:public_html/share
# name = '/jcr/data/car.npz'
# name = 'scan000.npz' # from thermolab dataset

def get_test_points(name='scan000'):
    ''' Valid names: scan000, car 
    All pointclouds should be in metres'''

    fname = name + '.npz'

    if not os.path.exists(tmp + fname):
        # get from website
        url = urllib2.urlopen(data_url + fname)
        localFile = open(tmp + fname, 'w')
        print 'Downloading test data:', url.geturl(), ' to ', tmp + fname
        localFile.write(url.read())
        localFile.close()

    xyzs = np.load(tmp + fname).items()[0][1]
    if name == 'car':
        xyzs[:, 1] = -xyzs[:, 1] # car is upside down so flip in y
    return xyzs

def convert_bremen_data(infname, outfname):
    # Extract x, y, and z columns from bremen city scan in txt file (infname)
    # Save extracted array to npz file (outfname)
    xyzs = np.loadtxt(infname, skiprows=1, usecols=(0, 1, 2))
    np.savez(outfname, xyzs)

def get_bremen_points():
    # TODO add script to download this data, where does it come from?
    fname = '/home/jdelmerico/data/bremen_city/scan000.npz' # Aligned points from ~1/2 of Bremen dataset
    print 'Loading bremen city data in ', fname
    scale = 1.0 # data in m?
    xyzs = np.load(fname).iteritems().next()[1] * scale 
    return xyzs

def get_colour_ply():
    ply_fname = tmp + '1305031102.160407.ply'
    if not os.path.exists(ply_fname):
        print 'Generating', ply_fname
        base = '../data/tum_dataset/'
        sys.path.append(base)
        import generate_pointcloud
        rgb_fname = base + '1305031102.175304.png'
        depth_fname = base + '1305031102.160407.png'
        generate_pointcloud.generate_pointcloud(rgb_fname, depth_fname, ply_fname)

    xyz_rgba = np.loadtxt(ply_fname, skiprows=11)
    return xyz_rgba[:,:6] # discard 7 column which is alpha
