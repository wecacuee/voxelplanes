import numpy as np
class OverflowError(RuntimeError):
    pass

def combine_normals(stats):
    ''' Combines multiple normal distributions specified by their statistics.
    Each item of stats consists of N, mean, covariance matrix N is number of
    points,  means and covariance matrices and returns the n, mean and
    covariance of the combination.'''

    # TODO use a structured array?
    Ns = np.array([i[0] for i in stats])
    means = np.array([i[1] for i in stats])
    covs = np.array([i[2] for i in stats])

    # Do nothing if all stats are empty
    if np.all(Ns == 0):
        return Ns, means, covs

    all_N = Ns.sum() # combined number
    assert all_N > 0, "Overflow can be the only reason" 
    # if all_N <= 0:
    #     raise OverflowError("Overflow occured")
    weights = np.float32(Ns)/all_N

    # combined weighted by number of points mean/centroid
    all_mean = np.dot(weights, means) 

    # combined cov see equation 4 in voxel planes paper
    all_cov = np.zeros((3,3))
    for i, (N, mean, cov) in enumerate(stats):
        all_cov += weights[i] * (cov + np.outer(mean, mean) - np.outer(all_mean, all_mean))

    # See combining error ellipses, equation 24, John E. Davis
    return all_N, all_mean, all_cov

def mean_cov(X):
    ''' Returns the number, mean and covariance matrix of set of points in n by
    3 format '''
    # cov default to normalising by N - 1 without ddof=0
    return len(X), np.mean(X, axis=0), np.cov(X.T, ddof=0) 

def test_combine_normas_zero():
    stats1 = (0, np.zeros(3), np.zeros((3,3)))
    X2 = np.random.rand(10, 3) 
    stats2 = mean_cov(X2)
    stats_all = tuple(stats2)
    stats_combined = combine_normals((stats1, stats2))
    assert stats_combined[0] == stats_all[0]
    assert np.allclose(stats_combined[1], stats_all[1])
    assert np.allclose(stats_combined[2], stats_all[2])


def test_combine_normals():
    X1 = np.random.rand(10, 3) 
    X2 = np.random.rand(10, 3) 
    stats1 = mean_cov(X1)
    stats2 = mean_cov(X2)
    stats_all = mean_cov(np.vstack((X1, X2)))
    # check combination of stats1 and stats2 equals stats_all
    stats_combined = combine_normals((stats1, stats2))
    assert stats_combined[0] == stats_all[0]
    assert np.allclose(stats_combined[1], stats_all[1])
    #print stats_combined[2]
    #print
    #print stats_all[2]
    assert np.allclose(stats_combined[2], stats_all[2])

if __name__ == '__main__':
    test_combine_normals()
    test_combine_normas_zero()
    print 'Guassian tests passed'
