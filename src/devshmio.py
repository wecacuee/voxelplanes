"""Methods related to writing and reading from /dev/shm"""
import fcntl

def dump2devshm(pickleable, key):
    """Dumps to dev shm with locking"""
    picklefile = "/dev/shm/%s.pickle" % key
    lockfile = "/dev/shm/%s.lock" % key
    lockfd = open(lockfile, 'w')
    try:
        fcntl.lockf(lockfd, fcntl.LOCK_EX)
        pickleable.dump(open(picklefile, "w"))
    finally:
        fcntl.lockf(lockfd, fcntl.LOCK_UN)

def loadfromdevshm(pickleable, key):
    """loads from dev shm with locking"""
    picklefile = "/dev/shm/%s.pickle" % key
    lockfile = "/dev/shm/%s.lock" % key
    lockfd = open(lockfile)
    try:
        fcntl.lockf(lockfd, fcntl.LOCK_SH)
        obj = pickleable.load(open(picklefile))
    finally:
        fcntl.lockf(lockfd, fcntl.LOCK_UN)
    return obj
