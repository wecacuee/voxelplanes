#! /usr/bin/python
''' Demonstration of finding plane patches within a voxel binned point cloud.
Requires: python python-numpy python-vtk python-mayavi2

Author: Julian Ryde
'''
import os
import time
import argparse
import numpy as np
import hashlib

import data
import cube
import voxelcloud
import pcl_reader as pclr
import gaussian
from utils import log

idx_n = voxelcloud.idx_n
idx_mean = voxelcloud.idx_mean
idx_cov = voxelcloud.idx_cov
idx_rgb = voxelcloud.idx_rgb

flt = np.float64
min_eig_ind = 0 # which of the three eigenvalues is the smallest
# evals are the variance in the direction of the associated eigenvector

def checksum(arr, chksum):
    ''' Performs an md5 checksum assert on the array arr ''' 
    #S = arr.sum(dtype=np.float64) # ensure accumulator has good precision
    S = hashlib.md5(arr).hexdigest()
    assert S == chksum, 'Array checksum failed. Expected: %s, Actual: %s' % (chksum, S)

def pca(xyzs): # TODO this function is copied from pca_edge
    '''Returns the centroid and eignvalues of PCA of the zero meaned xyzs.
    xyzs is an N by 3 numpy array of points'''
    # should I use matplotlib.mlab.PCA(a) instead?
    assert len(xyzs) > 2, 'Need at least 3 points for SVD'
    centroid = xyzs.mean(axis=0)
    U, S, V = np.linalg.svd(xyzs - centroid)
    # eigenvalues S
    # eigenvectors V, I think they are arranged horizontally namely each row of
    # V is an eigenvector
    return centroid, S, V

def pcacov(cov_mat):
    ''' Calculates pca on the covariance matrix '''
    #U, evals, evecs = np.linalg.svd(cov_mat)

    # seem to always be in ascending order see min_eig_ind
    evals, evecs = np.linalg.eigh(cov_mat) 

    # check the evals are sorted
    #assert np.all(np.argsort(evals) == np.array((0,1,2)))
    #evals = evals[::-1]
    #evecs = evecs[::-1]
    return evals, evecs.T 

def readpoints(input_file):
    _, ext = os.path.splitext(input_file)
    if ext == ".npz":
        xyzs = np.load(input_file)
        xyzs = xyzs['arr_0']
    elif ext == ".pcd":
        xyzs = pclr.readpcd(open(input_file))
        xyzs = np.vstack([xyzs[n] for n in xyzs.dtype.names]).T
    else:
        raise ValueError("Unknown extension %s" % ext)
    return xyzs


class L1PCA(object):
    def __init__(self, binned_point_cloud, pca_file):
        self.binned_point_cloud = binned_point_cloud
        ar = np.loadtxt(pca_file, dtype=flt)
        self.evecs = dict()
        self.evals = dict()
        for row in ar:
            v = tuple(row[:3].astype(np.int32))
            self.evecs[v] = row[3:12].reshape(3, 3)[::-1, :]
            self.evals[v] = row[15:11:-1]

    def l1pca(self, vertex):
        """Read L1 PCA by Rob platts output and return"""
        xyzs = self.binned_point_cloud.voxel_points_for_pca(vertex)
        centroid = np.mean(xyzs, axis=0) 
        v = tuple(vertex)
        if not self.evecs.has_key(v):
            return
        V = self.evecs[v]
        S = self.evals[v]
        return centroid, S, V

#    , use_sliding_window=True, l1pca_file=None):
#        if l1pca_file is not None:
#            self.pca_from_vertex = L1PCA(self, l1pca_file).l1pca
# 
#        if not use_sliding_window:
#            self.unique_indices = self.unique_indices_no_sliding_window
#            self.voxel_points_for_pca = \
#                    self.voxel_points_for_pca_no_sliding_window
#            # Always draw in case of no_sliding_window
#            self.should_draw = lambda xyzs, vertex: True
#

#def should_draw(self, xyzs, vertex):
#    return self.should_draw_by_nearby_points(xyzs, vertex) or \
#            self.should_draw_by_mean(xyzs, vertex)
#
#def should_draw_by_nearby_points(self, xyzs, vertex):
#    # check there are points near enough to this vertex within +/-
#    # resolution/2 
#    vertex_pos = self.unquantize(vertex)
#    near_inds = (xyzs < vertex_pos + self.res/2) & (xyzs > vertex_pos - self.res/2)
#    near_inds = np.all(near_inds, axis=1)
#    return np.any(near_inds)
#
#def should_draw_by_mean(self, xyzs, vertex):
#    vertex_pos = self.unquantize(vertex)
#    centroid = np.mean(xyzs, axis=0)
#    near_inds = (centroid < vertex_pos+ self.res/2) & (
#        centroid > vertex_pos - self.res/2)
#    return np.all(near_inds)

def voxel_points_for_pca_no_sliding_window(self, vertex):
    return self.bins[tuple(vertex)]

def unique_indices_no_sliding_window(self):
    occupancy_threshold = 2
    unique_inds = self.get_inds(occupancy_threshold)
    return unique_inds

def pca_from_vertex(voxels, vertex):
    nn_voxels = voxels.voxel_points_for_pca(vertex) 
    all_stats = []
    rgbs = []
    for V in nn_voxels:
        # TODO what about voxels that have fewer than 3 and therefore their
        # covariance matrices are all 0, could store the point coordinates in
        # the covariance matrix which you know from N

        rgbs.append(V[idx_rgb]) 
        # TODO how to combine colours of surrounding voxels? weighted mean
        if V[idx_n] > 2:
            all_stats.append((V[idx_n], V[idx_mean], V[idx_cov]))
    if len(all_stats) == 0:
        return None
    rgbs = np.array(rgbs)
    window_stats = gaussian.combine_normals(all_stats)
    # TODO do a proper color combination

    #xyzs = self.voxel_points_for_pca(vertex)
    #if not self.should_draw(xyzs, vertex):
        #return # skip if there are no points

    #if len(xyzs) == 2:
    #    # Hack to make the number of points 3
    #    for i in range(3 - len(xyzs)):
    #        xyzs = np.vstack((xyzs, centroid))

    # Compute pca by svd on covariance matrix
    evals, evecs = pcacov(window_stats[2])
    return window_stats[1], evals, evecs, np.uint8(rgbs.mean(axis=0))

def planes3(voxels, visualise=True, output_file=None):
    ''' Takes the voxelcloud voxels and considers the vertices of the grid.  For
    each vertex fit to 2x2x2 neighbourhood around each vertex but clip plane to
    single voxel centered on vertex.
    Returns the voxel vertices and corresponding centroid_evals_evecs
    
    Note that this primarily the meshification step and is only needed for
    visualisation so does not have to run as fast.'''

    start = time.time()
    ev_dt = np.dtype([('centroid', '<f4', 3),
                      ('evals', '<f4', 3), 
                      ('evecs', '<f4', (3, 3)),
                      ('rgbs', '<f4', 3),
                     ]) 

    # get the expanded by 2x2x2 set of unique voxels that need considering
    unique_inds = voxels.unique_indices()

    EV = np.empty(len(unique_inds), ev_dt)
    # initialise to nans so we can see which ones could not be calculated 
    EV['centroid'][:, 0] = np.nan 

    # TODO this loop is sloooooow
    for i, vertex in enumerate(unique_inds): # for each grid vertex 
        pca_tuple = pca_from_vertex(voxels, vertex)
        if pca_tuple is not None:
            EV[i] = pca_tuple

    # remove those that could not be calculated for various reasons
    keep_finite = np.isfinite(EV['centroid'][:,0]) # ones that are not nan

    # filter out lines of points, namely second eigen value is too small
    # eigenvalues are in ascending order
    # TODO this threshold should be a function of resolution???
    threshold = 0.01**2 # standard deviation of 2cm
    keep_planar = EV['evals'][:, 1] > threshold # TODO combine this into previous keep
    keep = np.logical_and(keep_finite, keep_planar)
    unique_inds = unique_inds[keep]
    EV = EV[keep]

    # classify into planes cylinders and other
    # large_count = np.sum(EV['evals'] > eval_threshold, axis=1)
    # large_count == 0 # small point clusters 
    # large_count == 1 # cylinders
    # large_count == 2 # planes 
    # large_count == 3 # spheres/blobs 

    # draw cylinders
    #cylinder_EV = EV[large_count == 1]
    #vertices = cylinder_EV['centroid']
    #directions = cylinder_EV['evecs'][:, 0, :] # maximum is first one
    #radii = 0.5 * np.sqrt(cylinder_EV['evals'][:, 1]) # radius is second largest?
    #visualiser.cylinders(vertices, directions, radii, length=res)

    # convert inds to vertices
    # unique_inds are actually the inds of the top right voxel of each 2x2x2
    # block so need to minus half a cell to get to the center of the 2x2x2 block
    # unquantize gives the center of the top right voxel
    vertices = voxels.unquantize(unique_inds) - voxels.res * 0.5

    taken = time.time() - start
    log('Number of bins: ', len(vertices))
    log('Bin processing rate (bins/s): ', int(len(vertices)/taken))
    log('Converting voxels planes to vtk mesh')

    #visualiser.show_points(xyzs)
    normals = EV['evecs'][:, min_eig_ind, :] 
    cube.show_bounded_planes(EV['centroid'], normals, vertices, voxels.res,
                          rgbs=EV['rgbs'], output_file=output_file, vis=visualise) # colour by stored colour
                          #rgbs=None) # colour by normal
    if visualise:
        import visualiser
        visualiser.show_window()
        visualiser.clear_window()

    return vertices, EV

if __name__ == '__main__':
    log('Starting')
    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile", nargs="?", help="input file path")
    parser.add_argument("-o", "--out", help="output file path")
    parser.add_argument("--l1pca", help="Use L1 PCA")
    parser.add_argument("--novis", dest='vis',
                        help="Disable visualization", default=True,
                        action="store_false")
    parser.add_argument("--no-sliding-window", dest='use_sliding_window',
                        help="Disable sliding window", default=True,
                        action="store_false")
    parser.add_argument("--res", help="Resolution of the voxel grid",
                        default=0.05, type=float)
    parser.add_argument("-R", help="Bounding box radius",
                        default=5, type=float)
    args = parser.parse_args()

    res = args.res

    if args.inputfile:
        xyzs = readpoints(args.inputfile)
    else:
        #xyzs = data.get_test_points('scan000') # car, scan000
        xyzs = data.get_colour_ply()

        # centralise the data
        com = xyzs[:,:3].mean(axis=0) 
        xyzs[:, :3] -= com

    log('Finished reading points from file')
        #xyzs = data.get_colour_ply() # xyz_rgbs
    # TODO test with both xyz and xyz_rgb arrays

    # reduce number of points to speed up running for debug 
    #lower, upper = (-1, -1, -1), (5, 1, 1)
    #filter_inds = np.all((xyzs > lower) & (xyzs < upper), axis=1)
    #xyzs = xyzs[filter_inds]

    # EV has already been calculated skip these for speed
    # TODO adjust dimensions to the min and max of data points?
    R = args.R
    vc = voxelcloud.Voxelcloud((-R, -R, -R), (R, R, R), resolution=res)
                           #use_sliding_window=args.use_sliding_window,
                           #l1pca_file=args.l1pca)
    log('Loading %d points into binned point cloud...' % len(xyzs))
    start = time.time()
    added = vc.addpoints(xyzs) # TODO accelerate point loading
    taken = time.time() - start
    log('Points added: ', added)
    log('Point load rate: ', int(added/taken), '/s')
    #vc.display() # check the voxel cloud looks right

    if args.vis:
        vc.display(show_window=False)

    log('Generating voxel planes')
    vertices, plane_EV = planes3(vc, visualise=args.vis, output_file=args.out)

    # some checksum regression tests for data.get_test_points('car')
    # checksum(vertices, '57cfd1f80de950b2cfa1d5bcbe4f5a51')
    # checksum(normals, 'a2d76ea60e0c8b11570694ef1178bfce')
    # checksum tests for xyzs = data.get_colour_ply() # xyz_rgbs
    checksum(vertices, 'e2306791c5d45a249e1b2a5488c1f219')
    checksum(plane_EV, 'f5e0df4a816aac3dd0581c960466d349')
    log('Finished')
