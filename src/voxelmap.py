import numpy as np

from voxelcloud import Voxelcloud, VoxelCloudData
from mapping import Plane2PointICP

class VoxelcloudMap(Voxelcloud):
    registration = Plane2PointICP()
    voxeltype = VoxelCloudData
    def __init__(self, *args, **kwargs):
        Voxelcloud.__init__(self, *args, **kwargs)

    def addpoints(self, xyz_rgbs, T=None):
        xyzs = xyz_rgbs[:, :3]
        if self.count_bins() > 0:
            T, err = self.registration.compute_transformation(self, xyzs, T)

        self.last_transform = np.eye(4) if T is None else T
        return Voxelcloud.addpoints(self, xyz_rgbs, T)

    def get_estimated_transform(self):
        return self.last_transform 

