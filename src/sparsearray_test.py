''' Thoroughly test sparse array by running through list methods and comparing
output to equivalent numpy dense method. This testing can be used against any
sparse implementation. '''

import time
import numpy as np
import sparsearray

variables = ['shape', 'ndim']
methods = ['sum',]
operators = ['__add__', '__sub__', '__mul__', '__div__',
'__truediv__',
'__floordiv__',
'__mod__',
'__pow__',
'__iadd__',
'__isub__',
'__imul__',
'__ipow__',
'__idiv__',
'__itruediv__',
'__ifloordiv__',
'__imod__',
'__gt__',
'__ge__',
'__lt__',
'__le__'
]

def _testequals(S, D):
    return np.allclose(S.dense(), D)

def _create_sparse():
    N = 100
    nondefault = 1000
    S = sparsearray.sparsearray((N,N), default = 3)
    # set some random elements
    #coords = np.random.randint(0, N, (nondefault, 2))
    #for ind in coords:
    #    S[ind[0], ind[1]] = 10

    # set all elements, this is for comparing speed with dense array access
    for i in range(N):
        for j in range(N):
            S[i, j] = 10
    return S


S1 = _create_sparse()
S2 = _create_sparse()

D1 = S1.dense()
D2 = S2.dense()
# find all methods and attributes common to both sparse and dense and test them
#print dir(S)

print 'Name\t\tlookup ratio\tstatus'
# test all operators
for operator in operators:
    print operator, '\t',
    S_method = getattr(S1, operator)
    D_method = getattr(D1, operator)

    start = time.time()
    sparse_result = S_method(S2)
    sparse_time = time.time() - start
    start = time.time()
    dense_result = D_method(D2)
    dense_time = time.time() - start
    print '%.2f' % (sparse_time/dense_time),
    assert _testequals(sparse_result, dense_result)
    print '\t\tPASS'

print
    #assert _testequals(S_method(D), D_method(D))

# test other methods
for method in methods:
    print method, '\t',
    S_method = getattr(S1, method)
    D_method = getattr(D1, method)
    assert S_method() == D_method()
    print 'PASS'

# test variables
for variable in variables:
    print variable, '\t',
    assert getattr(S1, variable) == getattr(D1, variable)
    print 'PASS'

# TODO test speed in comparison to dense arrays


#sum(self):
#dense(self):
#nondefault(self):

