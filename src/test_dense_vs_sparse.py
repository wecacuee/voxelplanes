import voxel_edge
import numpy as np
import scipy as sp
import scipy.ndimage as ndim
import mrol_mapping.visualiser.dispxyz as dispxyz
import mrol_mapping.poseutil as poseutil
import mrol_mapping.pointcloud as pointcloud
import mrol_mapping.occupiedlist as occupiedlist

import utils
import process_thermolab

map_res = 0.1
kernel_full_width = 5

if __name__ == "__main__":
    #import voxel_edge_test
    #xyzs = voxel_edge_test.generate_test_data()
    
    # get thermolab point cloud
    pc_generator = process_thermolab.thermolab_pointclouds()
    pc = pc_generator.next()

    ol = occupiedlist.OccupiedList(map_res)
    ol.add_points(pc.points) 
    xyzs = ol.getvoxels().astype(int) # hack to work around bug in sparsearray

    ''' Dense versions'''
    dense = voxel_edge.Structure_Tensor_Edge(xyzs, sparse=False)
    print repr(dense.pc_size)
    print 'Voxel dims:', dense.sizes
    print 'Voxel offsets: ', dense.offsets

    print 'Computing structure tensor ...'
    dense_inds, dense_vals = dense.computeStructureTensor(kernel_full_width)
    print 'Done'
    print 'Dense: inds ' + repr(dense_inds.shape) + ' vals ' + repr(dense_vals.shape)
    print dense_inds
    #raw_input('Press Enter')
    print dense_vals
    #raw_input('Press Enter')

    ''' Sparse version'''
    sparse = voxel_edge.Structure_Tensor_Edge(xyzs, sparse=True)
    print repr(sparse.pc_size)
    print 'Voxel dims:', sparse.sizes
    print 'Voxel offsets: ', sparse.offsets

    print 'Computing structure tensor ...'
    sparse_inds, sparse_vals = sparse.computeStructureTensor(kernel_full_width)
    print 'Done'
    print 'Sparse: inds ' + repr(sparse_inds.shape) + ' vals ' + repr(sparse_vals.shape)
    print sparse_inds
    #raw_input('Press Enter')
    print sparse_vals
    #raw_input('Press Enter')

    sparse_inds, sparse_vals = utils.sort_inds_vals(sparse_inds, sparse_vals)
    dense_inds, dense_vals = utils.sort_inds_vals(dense_inds, dense_vals)

    #assert utils.allclose(sparse_inds, sparse_vals, dense_inds, dense_vals)

    ''' Display the dense data by largest eigenvalue 
    C = dispxyz.colors(dense_vals[:,2])
    import visual
    fig = visual.display(title='Map',x=0,y=0,width=800,height=800,background=(1,1,1))
    scene = visual.display()
    MT = dispxyz.EnableMouseThread(scene)
    MT.start()
    dispxyz.showpts(dense_inds, color=C)
    scene.forward = (0,1,0)
    scene.up = (0,0,1)
    scene.forward = (0,0,-1)
    '''
    
    ''' Display the sparse data by largest eigenvalue
    C = dispxyz.colors(sparse_vals[:,2])
    import visual
    fig = visual.display(title='Map',x=0,y=0,width=800,height=800,background=(1,1,1))
    scene = visual.display()
    MT = dispxyz.EnableMouseThread(scene)
    MT.start()
    dispxyz.showpts(sparse_inds, color=C)
    scene.forward = (0,1,0)
    scene.up = (0,0,1)
    scene.forward = (0,0,-1)
    '''
