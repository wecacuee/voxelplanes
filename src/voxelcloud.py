import numpy as np
import time

import pointcloud
import data
import gaussian
import utils

import cPickle

import sparsearray # external library written by Julian Ryde numpy like sparse arrays
flt = np.float64

idx_n = 0
idx_mean = 1
idx_cov = 2
idx_rgb = 3
idx_normal = 4

# memoize if needed
def eigen(cov):
    w, v = np.linalg.eig(cov)
    return w,v

# TODO: Deprecated, no longer being used
def isplanar(cov, factor=10):
    """ If the eigen values w indicate that points inside this voxel are
    planar"""
    w, v = eigen(cov)
    w = np.abs(w)
    return (w[0] > factor * w[2]) and (w[1] > factor * w[2])

def compute_normal(cov):
    ''' Calculates the normal vector for the given covariance matrix 
    >>> cov = np.array([[  7.04538812e-06,  -1.43826713e-05,  -1.96525483e-06], \
                        [ -1.43826713e-05,   2.93612260e-05,   4.01193144e-06], \
                        [ -1.96525483e-06,   4.01193144e-06,   5.48192160e-07]]) 
    >>> compute_normal(cov)

    '''

    w, v = eigen(cov)
    idx_min = np.argmin(w)
    normal = v[:, idx_min]
    # if np.any(np.iscomplex(normal)): # TODO turn this into assert
    # import pdb; pdb.set_trace()
    return normal


class FlannCorrespondence(object):
    # Motivation: Nearest voxel may be occupied because of just noisy points. 
    # how can we ICP our points to the nearest voxel based on the number of
    # points? Should we find 8 nearest neighbors choose the one with highest
    # number of points or optimize a cost function based on weighted distance
    # from each of the voxels.
    """
    Computes neighbors by FLANN library using kd-tree algorithm.
    """
    def __init__(self):
        self._index_ready = False

    def _actually_build_index(self, xyzs):
        # points should be inside map boundaries
        xyzs_box = self.map.boxfilter(xyzs)
        voxel_ids = self.map.quantize(xyzs_box)

        voxels = self.map.voxels(voxel_ids) # get the voxel datastructure

        # consider the mean of voxels as point representation
        _, self.planar_voxels = voxels.planar_voxels()
        self.point_representation = self.planar_voxels.mean()

        self.flann.build_index(self.point_representation, algorithm="kdtree")
        self._index_ready = True

    def build_index(self, map):
        from pyflann import FLANN
        self.map = map
        self.flann = FLANN()
        self._index_ready = False

    def correspondence(self, xyzs):
        """ Returns the VoxelCloudData and points for which
        we were able to find correspondence"""
        if not self._index_ready:
            self._actually_build_index(xyzs)

        # result contains the indices of index points
        # dist is distance to them
        result, dist = self.flann.nn_index(xyzs, num_neighbors=1)
        return self.planar_voxels[result], xyzs

class VoxelOnlyCorrespondence(object):
    """
    If the new point is inside an occupied voxel, we have found a
    correspondence and it is the occupied voxel, otherwise give up
    on finding a correspondence.
    """
    def build_index(self, map):
        self.map = map

    def correspondence(self, xyzs):
        map = self.map
        # points should be inside map boundaries
        xyzs_box = map.boxfilter(xyzs)
        voxel_ids = map.quantize(xyzs_box)
        # voxels that are occupied
        # we are not looking in the neighborhood so far
        map_voxels = map.voxels(voxel_ids)
        occ_voxel_ids = map_voxels.planar_voxels()[0]
        return map_voxels[occ_voxel_ids], xyzs_box[occ_voxel_ids]

class VoxelCloudData(object):
    """Wrap the Voxel-cloud data structure.

    - It contains the minimum functionality to opaquely access self._data
    which is a numpy structured array, but users must not know this.
    - It do not contains any mapping functionality.
    - It never returns a structured array. Even when a subset is requested, it
    returns an object of itself. (See methods subset and flatten)
    """
    _n = 'n'
    _mean = 'mean'
    _cov = 'cov'
    _rgb = 'rgb'
    _normal = 'normals'
    data_dtype = [(_n, 'i8'),
             (_mean, 'f8', (3,)),
             (_cov, 'f8', (3, 3)),
             (_rgb, 'f8', (3,)), # mean of color means floats are better 
             (_normal, 'f8', (3,))
                 ]

    def __init__(self, dimensions, data=None):
        if data is None:
            # np.zeros or sparsearray.zeros
            # sparsearray is 10 times slower than numpy dense array
            self._data = sparsearray.zeros(dimensions, dtype=self.data_dtype)
        else:
            self._data = data

        self._planar_voxels_cache = None

        #self._data = (np.zeros(dimensions, dtype=self.data_dtype)
                      #if (data is None) else data)

    def addpoints(self, voxel_id, xyz_rgbs):
        self._planar_voxels_cache = None
        X = xyz_rgbs[:, :3]
        # Computation of stats do not belong here
        new_n = len(X)
        new_mean = np.mean(X, axis=0)
        new_cov = np.cov(X.T, ddof=0)
        # end of stat computation

        # TODO this does not belongs to this class?
        #new_normal = compute_normal(new_cov)
        #new_rgbs = self.compute_rgbs(new_normal, xyz_rgbs) 

        c_values = self._data[voxel_id]
        merged_n, merged_mean, merged_cov = \
                gaussian.combine_normals(
                    [(c_values[idx_n], c_values[idx_mean], c_values[idx_cov]),
                     (new_n, new_mean, new_cov)])

        # weighted color average
        old_n = c_values[idx_n]
        if xyz_rgbs.shape[1] == 6: # rgb coloured points
            new_rgb = np.mean(xyz_rgbs[:, 3:], axis=0)
            merged_rgbs = (c_values[idx_rgb] * old_n + new_rgb * new_n)/(old_n + new_n)
        else:
            merged_rgbs = compute_normal(merged_cov)

        merged_normals = compute_normal(merged_cov)

        # assign values
        self._data[voxel_id] = (merged_n, merged_mean, merged_cov,
                                merged_rgbs, merged_normals)

    def __len__(self):
        return self._data.shape[0]

    def __getitem__(self, voxel_id):
        if (len(voxel_id) == len(self._data.shape)
            and np.isscalar(voxel_id[0])):
            return self._data[voxel_id]
        else:
            return self.__class__(self._data.shape, self._data[voxel_id])

    def indices(self):
        dimensions = self._data.shape
        ndim = len(dimensions)
        grid = np.indices(dimensions)
        if ndim == 1:
            return grid[0]
        else:
            return grid.reshape(ndim, -1).T

    def nonzero_inds(self, occupancy_threshold=0):
        if isinstance(self._data , sparsearray.sparsearray):
            Ks, Vs = self._data.nondefault()
            Ns = np.array([V[0] for V in Vs])
            Ks = np.array(Ks)
            if occupancy_threshold == 0:
                return Ks
            else:
                return Ks[Ns > occupancy_threshold]
        elif isinstance(self._data, np.ndarray):
            xyz_ind = np.where(self._data[self._n] > occupancy_threshold)
            xyz_ind = np.vstack(xyz_ind)
            return xyz_ind.T.reshape(-1, len(self._data.shape))
        else:
            raise NotImplementedError("type({0}) not supported".format(
                type(self._data)))

    def nonzero(self):
        if isinstance(self._data , sparsearray.sparsearray):
            Ks, Vs = self._data.nondefault()
            return Vs
        elif isinstance(self._data, np.ndarray):
            return self._data[self._data[self._n] != 0]
        else:
            raise NotImplementedError("type({0}) not supported".format(
                type(self._data)))

    def planar_voxels(self):
        if isinstance(self._data, sparsearray.sparsearray):
            arr = self._data.dense()
        else:
            arr = self._data
        norms = arr[self._normal]
        planar_cond = np.all(np.isreal(norms), axis=1)
        planar_ids = np.where(planar_cond)
        return planar_ids, self.__class__(None, arr[planar_cond])

    def normals(self):
        if isinstance(self._data, sparsearray.sparsearray):
            arr = self._data.dense()
        else:
            arr = self._data
        return arr[self._normal]

    def mean(self):
        if isinstance(self._data, sparsearray.sparsearray):
            arr = self._data.dense()
        else:
            arr = self._data
        return arr[self._mean]

    def dump(self, fd):
        cPickle.dump(self._data, fd, cPickle.HIGHEST_PROTOCOL) 

    @classmethod
    def load(cls, fd):
        self = cls(None, data=cPickle.load(fd))
        return self


# TODO merge with BinnedPointcloud class in pca_edge.py
class Voxelcloud(object):
    ''' A dense object numpy array where each cell contains the number, mean and
    covariance of the points within that cell.'''
    # TODO add keep points flag to disable self.bins
    # generate the offsets for a 2x2x2 neighbourhood
    # it maps from the bottom left voxel to give the voxel indices of all the
    # voxels in the 2x2x2 neighbourhood
    offsets = np.indices((2, 2, 2)).astype(np.int16)
    offsets.shape = 3, -1
    offsets = offsets.T 
    #self.offsets = offsets # remember it 
    voxeltype = VoxelCloudData

    def __init__(self, mins, maxs, resolution,
                 correspondence_algo=VoxelOnlyCorrespondence()):
        ''' mins are the minimum x, y, z and maxs are the max, resolution '''
        self.mins, self.maxs = np.array(mins), np.array(maxs)
        self.res = np.asarray(resolution)
        D = np.int32(np.ceil((self.maxs - self.mins)/ resolution))
        self.correspondence_algo = correspondence_algo
        self.dimensions = D
        try:
            self.bins = self.voxeltype(D)
        except MemoryError as me:
            # bytes needed for each voxel
            cell_mem = np.empty(1, self.voxeltype.data_dtype).nbytes
            print 'Array size:', D
            print 'Estimated memory needed (MB):', np.prod(D)*cell_mem*1e-6
            raise me


        #SortedBinning.__init__(self, np.vstack((self.mins, self.maxs)).T,
                              #[resolution] * len(self.mins))

    def __repr__(self):
        return str(self.bins) + ', resolution: ' + str(self.res) \
                + ', bounds: ' + str(self.mins) + str(self.maxs)
    
    def count_bins(self, occupancy_threshold=0):
        return len(self.bins.nonzero())

    def get_inds(self, occupancy_threshold=0):
        ''' Returns an array of the indices with occupancy greater than
        specified threshold '''
        return self.bins.nonzero_inds()

    def get_iterator(self, occupancy_threshold=0):
        ''' Returns all voxels with occupancy (point count) greater than the
        specified threshold  '''
        inds = self.get_inds(occupancy_threshold)
        for row in inds:
            yield self.bins[row[0], row[1], row[2]]

    def unquantize(self, inds):
        ''' Returns the xyz value corresponding to the array index for this
        occupancygrid
        >>> vc = Voxelcloud((0,0,0), (1,1,1), 0.1)
        >>> vc.unquantize( [(5,5,5), (5,6,7)] )
        array([[ 0.55,  0.55,  0.55],
               [ 0.55,  0.65,  0.75]])

        '''
        return inds * self.res + self.mins + 0.5 * self.res

    def quantize(self, xyzs):
        ''' Returns the array indices for the Nx3 xyz points and so should
        always be positive 
        >>> vc = Voxelcloud((0,0,0), (1,1,1), 0.1)
        >>> vc.quantize([(0.51, 0.52, 0.59), (0,0,0)])
        array([[5, 5, 5],
               [0, 0, 0]], dtype=int32)

        Round trip process should give values as close to original ones as
        possible.
        >>> vc.unquantize(vc.quantize( (0.55, 0.52, 0.57) ))
        array([ 0.55,  0.55,  0.55])

        '''
        return np.int32((xyzs - self.mins)/self.res)

    def boxfilter(self, xyzs):
        return pointcloud.boxfilter(xyzs, self.mins, self.maxs)

    def voxels(self, inds=None):
        if inds is None:
            # returns VoxelCloudData of sparse array
            return self.bins
        else:
            # uses VoxelCloudData.__getitem__ to get a voxel cloud data of
            # dense array
            return self.bins[inds[:, 0], inds[:, 1], inds[:, 2]]

    def readpoints(self, xyzs): 
        """
        return voxels containing the points

        @param xyzs: list of points
        @type xyzs: n x 3 ndarray
        """
        # points should be inside map boundaries
        xyzs_box = self.boxfilter(xyzs)
        voxel_ids = self.quantize(xyzs_box)
        # voxels that are occupied
        # we are not looking in the neighborhood so far
        map_voxels = self.voxels(voxel_ids)
        return map_voxels

    def update_correspondence_index(self):
        self.correspondence_algo.build_index(self)

    def correspondence(self, xyzs):
        return self.correspondence_algo.correspondence(xyzs)

    def addpoints(self, xyz_rgbs, T=None):
        ''' Points should be xyz or xyzrgb format and T is some transform to
        apply to the points before adding them.'''
        cols = len(xyz_rgbs[0])
        assert cols == 3 or cols == 6, 'Incorrect number of columns for points: ' + str(cols)
        #xyzs = xyz_rgbs[:, :3]
        
        if T is not None:
            xyz_rgbs[:, :3] = utils.apply_transform(xyz_rgbs[:, :3], T)

        xyz_rgbs = self.boxfilter(xyz_rgbs)
        # No points, nothing to do
        npoints = xyz_rgbs.shape[0]
        if npoints < 1:
            return npoints
        abcs = self.quantize(xyz_rgbs[:, :3])

        for ind, voxel_xyz_rgbs in pointcloud.grouped_iterator(xyz_rgbs, abcs):
            ind = tuple(ind) # turn ind into tuple for convenient array access
            self.bins.addpoints(ind, voxel_xyz_rgbs)

        return npoints

    def __len__(self):
        ''' Returns the number of voxels in this voxelcloud '''
        return self.count_bins()

    def unique_indices(self): # TODO does this really belong in this class
        occupancy_threshold = 2
        inds = self.get_inds(occupancy_threshold)

        # fast way of adding all the offsets to each of row of inds
        #expanded_inds = self.offsets[:, np.newaxis, :] + inds
        #expanded_inds = expanded_inds.reshape(-1, 3)
        # do the packing trick TODO

        # generate the grid vertices that need considering
        # remove duplicates
        # TODO this method is likely to be slow

        # this helps to fill holes
        unique_inds = set()
        for ind in inds:
            for row in ind + self.offsets:
                unique_inds.add(tuple(row))
        unique_inds = np.array(tuple(unique_inds))
        return unique_inds

    def voxel_points_for_pca(self, vertex):
        ''' Return the non-empty voxels  in the 2x2x2 neighbourhood of the
        given vertex '''
        voxels = []
        for offset in self.offsets:
            cell_ind = tuple(vertex - offset)
            try:
                V = self.bins[cell_ind]
                #xyzs.extend(self.bins[cell_ind])
                voxels.append(V)
            except IndexError: # offset outside of array
                continue
        #xyzs = np.array(xyzs) 
        return voxels

    def points(self):
        ''' Returns points corresponding to the centroid of then points that
        have fallen in each voxel and their colours 

        Also see: Voxelcloud.correspondence()
        '''
        return 

    def voxel_ids(self):
        xyz_ind = np.indices(self.dimensions)
        return xyz_ind.T.reshape(-1, len(self.dimensions))

    def display(self, show_window=True):
        ''' Display the means of each voxel '''
        import visualiser # this is very slow so only import if needed
        # TODO draw cubes for each voxel rather than points?
        xyzs = []
        rgbs = []
        for V in self.get_iterator():
            xyzs.append(V[idx_mean])
            rgbs.append(V[idx_rgb])
        xyzs = np.array(xyzs)
        rgbs = np.array(rgbs, dtype=np.uint8) # TODO make sure rgb is stored as uint8

        visualiser.show_points(xyzs[:,:3], rgbs)
        if show_window:
            visualiser.show_window()

    def dump(self, fd):
        cPickle.dump(self.mins, fd)
        cPickle.dump(self.maxs, fd)
        cPickle.dump(self.res , fd)
        self.bins.dump(fd)

    @classmethod
    def load(cls, fd):
        mins = cPickle.load(fd)
        maxs = cPickle.load(fd)
        res  = cPickle.load(fd)
        self = cls(mins, maxs, res)
        self.bins = self.voxeltype.load(fd)
        return self

if __name__ == '__main__':

    # TODO add some regression tests
    # Do it twice because some imports take a long time?

    R = 5
    res = 0.05
    vc = Voxelcloud((-R, -R, -R), (R, R, R), resolution=res)

    xyzs = data.get_test_points('car') # car, scan000
    # random data
    #np.random.seed(6)
    #xyzs = np.random.rand(10000, 3)

    print 'Loading points into binned point cloud...'
    start = time.time()
    vc.addpoints(xyzs) # TODO accelerate point loading
    taken = time.time() - start
    added = len(vc)
    print 'Points added:', added
    print 'Point load rate:', int(added/taken), '/s'

    vc_iter = vc.get_iterator()
    print
    print 'Showing first few voxels'
    for i in range(5):
        V = vc_iter.next()
        S = (V[idx_n], V[idx_mean], V[idx_cov])
        print V
        #print V.points
        print S[0], S[1]
        print S[2]
        print
    # quick simple checksum test for regression  
    chksum = 16097.280058141805
    assert vc.means().sum() == chksum, 'Voxel cloud failed'
