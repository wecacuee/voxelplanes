import numpy as np
import scipy
import pointcloud
from utils import apply_transform
import cube
import visualiser
import time
import utils
import voxelcloud

DEBUG_ICP = False

class VoxelMap(object):
    """Stores the map
    """
    def addpoints(self, xyzs):
        """
        add points to the map with transformation T
        """
        pass

    def quantize(self, xyzs):
        """Returns voxel ID for bunch of points
        """
        pass

    def voxels(self, voxel_ids=None):
        """Voxel objects for each voxel IDs

        if voxel_ids is None, return all voxels

        @return voxels: Ducktype Voxel
        """
        pass

def solvesymmetric_cholesky(A, b):
    L = np.linalg.cholesky(A)
    y = scipy.linalg.solve_triangular(L, b, lower=True)
    x = scipy.linalg.solve_triangular(L.H, y, lower=False)
    return x

def solvesymmetric_simple(A, b):
    return np.real(np.linalg.lstsq(A, b)[0])

def downsample(xyzs, res):
    inds = np.int16(xyzs / res)
    uniq, P, xyzs = pointcloud.group_by_voxel(xyzs, inds)
    return uniq * res

class Plane2PointICP(object):
    """
    This mapping algorithm should be independent of the data structure we use
    for . May be ICP or thresholded ICP.
    """

    solvesymmetric = lambda self, *args : solvesymmetric_simple(*args)
    """Solve linear system of equations given that A is a real symmetric
    matrix
    """

    def __init__(self, downsample_res_to_map_res=0):
        self._downsample_res_to_map_res = downsample_res_to_map_res

    def compute_transformation(self, map_, xyzs, initial_pose=None):
        """Adds points `xyzs` to `map_` after ICP alignment

        @type map_: like VoxelMap
        @param xyzs: list of points
        @type xyzs: n x 3 ndarray
        @param initial_pose: Homogeneous transformation
        @type initial_pose: 4x4 ndarray 
        """

        # downsample
        if self._downsample_res_to_map_res > 0:
            downsample_res = map_.res * self._downsample_res_to_map_res
            xyzs = downsample(xyzs, res=downsample_res)
        npoints = len(xyzs)
        start = time.time()

        # can be slow (builds kdtree for nearest neighbor matching)
        map_.update_correspondence_index()

        preverr = np.inf
        err = self._compute_error(map_, xyzs, initial_pose)
        T = initial_pose
        iterations = 0
        while err <= preverr:
            preverr = err
            prevT = T
            T, err = self._compute_transformation(map_, xyzs, T)
            iterations += 1

        print("Iterations : %d, Time: %f, Pts: %d, Prev err/pt: %f; Error/pt: %f" %
              (iterations, time.time() - start, npoints, preverr, err))
        return prevT, preverr

    def _compute_error(self, map_, xyzs, initial_pose=None):
        if initial_pose is not None:
            xyzs = apply_transform(xyzs, initial_pose)
        voxels, points = self._compute_correspondence(map_, xyzs)
        if len(voxels) == 0:
            print("No correspondence found")
            return np.inf
        means = voxels.mean()
        normals = voxels.normals()
        errors = np.sum((points - means) * normals, axis=1)
        return np.mean(np.abs(errors))

    def _compute_correspondence(self, map_, xyzs):
        """
        Return the correspondence points/planes in the map

        @type map_: like VoxelMap
        """
        return map_.correspondence(xyzs)

    def _compute_transformation(self, map_, xyzs, initial_pose=None):
        assert len(xyzs.shape) == 2, str(xyzs.shape)
        assert xyzs.shape[1] == 3
        if initial_pose is not None:
            xyzs = apply_transform(xyzs, initial_pose)

        voxels, points = self._compute_correspondence(map_, xyzs)
        if len(voxels) == 0:
            print("No correspondence found")
            return np.eye(4), np.inf if initial_pose is None else initial_pose
        #npoints = len(points)

        # See Szymon Rusinkiewicz's "A quick writeup on deriving the equations
        # of ICP and analyzing their stability for different surface shapes."
        # http://www.cs.princeton.edu/~smr/papers/icpstability.pdf

        # c_i = p_i x n_i
        normals = voxels.normals()
        cross_prods = np.cross(points, normals)

        # cn_i = [c_ix, c_iy, c_iz, n_ix, n_iy, n_iz]
        cns = np.hstack((cross_prods, normals))
        cncov = cns.T.dot(cns)

        # b = - \sum_i cn_i ( (p_i - q_i)^T n_i )
        means = voxels.mean()
        errors = np.sum((points - means) * normals, axis=1)
        if DEBUG_ICP: # draw
            pointofcontact = points - errors.reshape(-1, 1) * normals
            pts = visualiser.show_points(points)
            lines = visualiser.show_lines(points, pointofcontact)
            visualiser.show_window()
            visualiser.remove_actor(lines)
            visualiser.remove_actor(pts)

        b = - np.sum(cns * errors.reshape(-1,1), axis=0)
        x = self.solvesymmetric(cncov, b)
        T = np.identity(4)
        T[:3, 3] = x[3:]
        alpha, beta, gamma = x[:3]
        T[:3, :3] = [[1, -gamma, beta],
                     [gamma, 1, -alpha],
                     [-beta, alpha, 1]]
        retT =  T if (initial_pose  is None) else np.dot(T, initial_pose)
        return retT, np.mean(np.abs(errors))

class VoxelCloudData(voxelcloud.VoxelCloudData):
    def normal(self):
        normals = np.empty(self._data.shape, dtype=[('normals', 'f8', (3,))])
        for i in range(self._data.flatten().shape[0]):
            norm = voxelcloud.compute_normal(self._data[i][2])
            normals[i] = np.array(norm)
        return normals['normals']

class DummyVoxelICPMap(VoxelMap, voxelcloud.Voxelcloud):
    registration = Plane2PointICP()
    voxeltype = VoxelCloudData
    correspondence_algo = voxelcloud.VoxelOnlyCorrespondence()
    def __init__(self, data, res, min_, max_):
        self.data = self.voxeltype(data.shape, data)
        self.res = res
        self.min_ = min_
        self.max_ = max_

    def boxfilter(self, xyzs):
        return pointcloud.boxfilter(xyzs, self.min_, self.max_)

    def quantize(self, xyzs):
        return np.int64((xyzs - self.min_) / self.res)

    def unquantize(self, voxel_ids):
        return np.float32(voxel_ids * self.res) + self.min_

    def voxels(self, voxel_ids=None):
        if voxel_ids is None:
            return self.data
        else:
            return self.data[voxel_ids[:, 0], voxel_ids[:, 1], voxel_ids[:, 2]]

    def addpoints(self, xyzs, initial_pose=None):
        T, err = self.registration.compute_transformation(
            self, xyzs, initial_pose)
        xyzs = apply_transform(xyzs, T)
        VoxelMap.addpoints(self, xyzs)


###
# Testing
###

def sample_planar_points(npts, pt, normal, ranges):
    points = np.empty((npts, len(normal)))
    idxsorted = np.argsort(np.abs(normal))
    partialdot = np.zeros(npts)
    for idx in idxsorted[:-1]:
        range_idx = ranges[idx]
        points[:, idx] = np.random.uniform(range_idx[0], range_idx[1], npts)
        partialdot += points[:, idx] * normal[idx]

    lastidx = idxsorted[-1]
    points[:, lastidx] = np.dot(pt, normal) - partialdot / normal[lastidx]
    return points

def create_dummy_map():
    """
    9 x 9 map
     3+----------+----------+----------+
      |          |          |          |
      |          |----------|          |
      |          |          |          |
     2+----------+----------+----------+
      |   /      | oooooooo |    \     |
      |  /       | oooooooo |     \    |
      | /        | oooooooo |      \   |
     1+----------+----------+----------+
    ^ |          |          |          |
    | |          |          |          |
    y |          |          |          |
      +----------+----------+----------+
     0           1          2          3
         x ->
    """
    points = np.empty((800, 3))
    points[:200, :] = sample_planar_points(200, (0.5, 1.5, .5), (-1, 1, 0),
                                          ((0, 1), (1, 2), (0, 1)))
    points[200:400, :] = sample_planar_points(200, (1.5, 1.5, .5), (0, 0, 1),
                                          ((1, 2), (1, 2), (0, 1)))
    points[400:600, :] = sample_planar_points(200, (2.5, 1.5, .5), (1, 1, 0),
                                          ((2, 3), (1, 2), (0, 1)))
    points[600:800, :] = sample_planar_points(200, (1.5, 2.5, .5), (0, 1, 0),
                                          ((1, 2), (2, 3), (0, 1)))
    visualiser.show_axes(scale=1)
    visualiser.show_points(points)
    visualiser.show_window()
    visualiser.clear_window()
    res = np.array((.5, .5, .5))
    min_ = np.array((0, 0, 0))
    max_ = np.array((3, 3, 1))
    map_ = voxelcloud.Voxelcloud( min_, max_, res)
    map_.addpoints(points)
    return map_

def create_dummy_pointcloud(map_):
    voxels = map_.voxels()
    ids, npts, mean, cov, rgbs = voxels.planar_voxels()
    xyzs = np.empty((0, 3))
    for n, mu, sig in zip(npts, mean, cov):
        p = np.random.multivariate_normal(mu, sig, int(n))
        xyzs = np.vstack((xyzs, p))
    return xyzs

def test_icp():
    map_ = create_dummy_map()
    xyzs = create_dummy_pointcloud(map_)
    T = np.identity(4)
    T[:3, 3] = np.random.random(3)/4.
    alpha, beta, gamma = np.random.random(3) / 10.
    T[:3, :3] = [[1, -gamma, beta],
                 [gamma, 1, -alpha],
                 [-beta, alpha, 1]]
    print T
    xyzs = apply_transform(xyzs, T)
    registration = Plane2PointICP()

    # points should be inside map boundaries
    xyzs_box = map_.boxfilter(xyzs)
    voxel_ids = map_.quantize(xyzs_box)
    map_voxels = map_.voxels(voxel_ids)
    ids, npts, positions, cov, rgbs = map_voxels.planar_voxels()
    normals = []
    for c in cov:
        normals.append(voxelcloud.compute_normal(c))
    normals = np.asarray(normals)
    cube_centers = map_.unquantize(map_.quantize(np.asarray(positions)))
    cube.show_bounded_planes(positions, normals, cube_centers, 
                             map_.res, rgbs=utils.rgbs_by_normals(normals))
    pts = visualiser.show_points(apply_transform(xyzs, T))
    visualiser.show_window()
    visualiser.remove_actor(pts) # removes points

    T_est, err = registration.compute_transformation(map_, xyzs)
    visualiser.show_points(apply_transform(xyzs, T_est))
    visualiser.show_window()
    print "Test:", T_est
    print("This test never succeeded. don't worry about it. Just check the"
          + " alignment visually for now")

if __name__ == '__main__':
    print("Please run scripts/test_mapping_freiburg.py to test this module")

