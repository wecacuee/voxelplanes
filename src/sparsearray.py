"""
Module for sparse arrays using dictionaries, original motivation and high
performance code from occupiedlist.py in MROL mapping on launchpad a library
for mobile robot localization and mapping.

Where available usage should be identical to numpy.array.

Code and motivation was also taken from the following
ndsparse (https://launchpad.net/ndsparse) by Pim Schellart
sparray by Jan Erik Solem, Feb 9 2010.

Author: Julian Ryde
"""

import numpy as np
import operator
debug = False

def isiterable(X):
    try:
        iter(X)
    except TypeError:
        return False # not iterable
    else:
        return True # iterable


# def ones(shape, dtype=float): # TODO implement ones
def zeros(shape, dtype=float):
    ''' See numpy.zeros

    >>> zeros(5)
    array([ 0.,  0.,  0.,  0.,  0.])
    >>> zeros((2, 1))
    array([[ 0.],
           [ 0.]])

    >>> s = (2,2)
    >>> zeros(s)
    array([[ 0.,  0.],
           [ 0.,  0.]])

    >>> dt = np.dtype([('f1', np.int16)])
    '''
    shape = np.atleast_1d(shape)
    sa = sparsearray(shape, dtype=dtype)
    return sa

class sparsearray(object):
    """ Class for n-dimensional sparse array objects.  Should mimic the
    behaviour of numpy array as much as possible.  

    >>> S = sparsearray((3, 3), default=1)
    >>> S[1, 0] = 10

    Single element access
    >>> S[1, 0], S[1, 2]
    (10.0, 1.0)

    Multiple indices
    >>> S[(1, 0, 0), (0, 2, 0)]
    array([ 10.,   1.,   1.])

    Slice access
    >>> S[1:3, 0:2]
    array([[ 10.,   1.],
           [  1.,   1.]])

    >>> S[1, :]
    array([ 10.,   1.,   1.])

    Setting single element
    >>> S = zeros((2,3), dtype=int)
    >>> S[1, 0] = 9; S
    array([[0, 0, 0],
           [9, 0, 0]])

    Different default
    >>> sparsearray((2, 3), default=1, dtype=int)
    array([[1, 1, 1],
           [1, 1, 1]])

    Setting multiple elements with multiple values
    >>> S = zeros((2,3), dtype=int)
    >>> S[(1, 0), (1, 2)] = (1, 2); S
    array([[0, 0, 2],
           [0, 1, 0]])

    Setting multiple elements with single value
    >>> S = zeros((2,3), dtype=int)
    >>> S[(1, 0), (1, 2)] = 7; S
    array([[0, 0, 7],
           [0, 7, 0]])

    Sparse vectors are supported
    >>> S = zeros(3, int); S
    array([0, 0, 0])
    >>> S[0]
    0
    >>> S[[0,1]]
    array([0, 0])

    Handles custom data types
    >>> dt = np.dtype([('a', np.int32), ('b', np.float32)])
    >>> S = zeros(1, dtype=dt); S
    array([(0, 0.0)], 
          dtype=[('a', '<i4'), ('b', '<f4')])

    >>> S[0][0] = 1; S
    array([(1, 0.0)], 
          dtype=[('a', '<i4'), ('b', '<f4')])

    """

    def __init__(self, shape, default=None, dtype=float):
        
        # set up default value of non-assigned elements
        if default is None:
            # this handles complex compound dtypes
            self._default = np.zeros(1, dtype=dtype)[0] 
        else:
            # default should be the same type as self.dtype
            self._default = dtype(default) 

        self.shape = tuple(shape)
        self.ndim = len(shape)
        self.dtype = dtype
        self._data = {}

    def _set_one(self, index, value):
        try:
            # value needs to be cast to same dtype as self
            #self._data[tuple(index)] = self.dtype(value)
            self._data[index] = self.dtype(value)
        except TypeError: # it is complex dtype so casting fails
            self._data[tuple(index)] = value

    def __setitem__(self, indexes, values):
        ''' set value to position given in index, where index is a tuple of
        length ndim '''

        # indexes can either be 1D or 2D, 1D means setting a single element and
        # 2D means setting multiple elements value on the other hand should be
        # of dtype?

        ind_ndim = np.array(indexes).ndim # could be 0, 1, 2
        # TODO use np.isscalar and np.isvector instead?
        if ind_ndim == 0: # single number
            self._set_one(indexes, values)
        elif ind_ndim == 1: # vector of numbers
            self._set_one(tuple(indexes), values)
        else: # addressing multiple elements
            # TODO check the number of values either one or the same number as
            # indexes
            indexes = np.array(indexes).T

            if isiterable(values): # multiple indexes multiple values
                for index, value in zip(indexes, values):
                    self._set_one(index, value)
            else: # multiple indexes single value
                for index in indexes:
                    self._set_one(index, values) # values is only one

		# TODO finish fancy indexing for setting array elements

    def _get_one(self, index):
        return self._data.get(index, self._default)

    def _get_many(self, indices):
        # indices has to be N by ndim 
        ret = np.empty(len(indices), self.dtype)
        for i, ind in enumerate(indices):
            ret[i] = self._get_one(tuple(ind))
        return ret

    #def squeeze(self): # TODO implement

    def __getitem__(self, indexes):
        ''' Get combinations of elements with fancy indexing.  '''

        ind_ndim = np.array(indexes).ndim # could be 0, 1, 2
        try:
            if ind_ndim == 0: # single number
                return self._get_one(indexes)
            elif ind_ndim == 1: # vector of numbers
                return self._get_one(tuple(indexes))
            else:
                indexes = np.array(indexes, copy=False)
                return self._get_many(indexes.T)
        except TypeError: # needs complicated slice access
            pass

        # slice access
        length, dims = len(indexes), len(self.shape)
        #  Fix indexes, handling ellipsis and incomplete slices.
        fixed = []
        for slice_ in indexes:
            if slice_ is Ellipsis:
                fixed.extend([slice(None)] * (dims-length+1))
                length = len(fixed)
            elif isinstance(slice_, (int, long)):
                fixed.append(slice(slice_, slice_+1, 1))
            elif isinstance(slice_, slice):
                fixed.append(slice_)
            else:
                raise IndexError(str(type(slice_))+' object not supported for indexing')

        # expand the slices taking into account the length of each dimension
        # e.g. : will get expanded to 0:5 if 5 is length of that dimension
        expanded = [F.indices(S) for F, S in zip(fixed, self.shape)]
        expanded = [slice(E[0], E[1], E[2]) for E in expanded]

        # Generate all indices from slices
        inds = np.mgrid[expanded]

        ret_shape = inds.shape[1:]
        # reshape indices into a vertical list of indices
        inds.shape = self.ndim, -1 
        inds = inds.T

        # make and populate dense array for return 
        # TODO problem with this if you want to return sparse arrays when
        # indexing into a sparse array
        ret = np.empty(ret_shape, dtype=self.dtype)
        ret[:] = self._default
        offset_inds = inds - inds[0]
        ret[offset_inds[:, 0], offset_inds[:, 1]] = self._get_many(inds)

        # make and populate sparse array for return 
        # user can always make it dense if required
        # currently does not work!
        #ret = sparsearray(ret_shape, default=self._default) 
        #ret.__setitem__(inds - inds[0], self._get_many(inds))
        return ret.squeeze()

    def __delitem__(self, index):
        """ index is tuples of element to be deleted. """
        if self._data.has_key(index):
            del(self._data[index])
            
    def __operate2__(self, other, operator, inplace=False):
        # TODO iterate through the smallest
        # what about with a dense array?
        if inplace:
            out = self
        else:
            out = self.__class__(self.shape, self.dtype)
            out._data = self._data.copy()

        for inds, val in self.iteritems():
            pass

        out._default = operator(self._default, other._default)
            
    def __operate__(self, other, operator, inplace=False):
        # convert a single number to sparse array
        if not hasattr(other, 'shape'):
            other = self.__class__(self.shape, default=other)
            # Both sparse arrays have exactly same non default keys
            for K in self._data.viewkeys():
                other._data[K] = other._default

        if self.shape != other.shape:
            raise ValueError('Array sizes do not match. '+str(self.shape)+' versus '+str(other.shape))

        if inplace:
            out = self
        else:
            out = self.__class__(self.shape, dtype=self.dtype)
            out._data = self._data.copy()

        out_data = out._data 

        self_default = self._default
        self_data = self._data

        other_data = other._data
        other_default = other._default

        ## operate on elements only in self
        #for K in self._data.viewkeys() - other._data.viewkeys():
        #    out_data[K] = operator(self_data[K], other_default)

        ## operate on elements only in other
        #for K in other._data.viewkeys() - self._data.viewkeys():
        #    out_data[K] = operator(self_default, other_data[K])

        ## operate on elements in both
        #for K in other._data.viewkeys() & self._data.viewkeys():
        #    out_data[K] = operator(self_data[K], other_data[K])

        # operate on elements that are in either self, other
        for K in self._data.viewkeys() | other._data.viewkeys():
            out_data[K] = operator(self_data[K], other_data[K])

        # operate on elements that are default in both
        out._default = operator(self._default, other._default)

        # TODO what about elements that become the default value?

        return out

    def __add__(self, other):
        """ Add two arrays. 
        >>> S = sparsearray((3, 3), default=1)
        >>> S[1, 1] = 10
        >>> (S + S).dense()
        array([[  2.,   2.,   2.],
               [  2.,  20.,   2.],
               [  2.,   2.,   2.]])
        """
        
        return self.__operate__(other, operator.add)

    def __sub__(self, other):
        """ Subtract two arrays. """
        return self.__operate__(other, operator.sub)

    def __mul__(self, other):
        """ Multiply two arrays (element wise). """
        return self.__operate__(other, operator.mul)
        
    def __div__(self, other):
        """ Divide two arrays (element wise). 
            Type of division is determined by dtype. """
        return self.__operate__(other, operator.div)

    def __truediv__(self, other):
        """ Divide two arrays (element wise). 
            Type of division is determined by dtype. """
        return self.__operate__(other, operator.truediv)
        
    def __floordiv__(self, other):
        """ Floor divide ( // ) two arrays (element wise). """
        return self.__operate__(other, operator.floordiv)
        
    def __mod__(self, other):
        """ mod of two arrays (element wise). """
        return self.__operate__(other, operator.mod)
        
    def __pow__(self, other):
        """ power (**) of two arrays (element wise). """
        return self.__operate__(other, operator.pow)

    def __iadd__(self, other, inplace=True):
        return self.__operate__(other, operator.add, inplace=True)

    def __isub__(self, other):
        return self.__operate__(other, operator.sub, inplace=True)

    def __imul__(self, other):
        return self.__operate__(other, operator.mul, inplace=True)
        
    def __idiv__(self, other):
        return self.__operate__(other, operator.div, inplace=True)
        
    def __itruediv__(self, other):
        return self.__operate__(other, operator.truediv, inplace=True)
        
    def __ifloordiv__(self, other):
        return self.__operate__(other, operator.floordiv, inplace=True)
        
    def __imod__(self, other):
        return self.__operate__(other, operator.mod, inplace=True)

    def __ipow__(self, other):
        return self.__operate__(other, operator.pow, inplace=True)
        
    def __repr__(self):
        return repr(self.dense())

    def __str__(self):
        return str(self.dense())

    def __gt__(self, other):
        '''
        >>> S = zeros(3, dtype=int)
        >>> S[0] = 2
        >>> S > 0
        array([ True, False, False], dtype=bool)

        '''
        return self.__operate__(other, operator.__gt__, inplace=False)

    def __ge__(self, other):
        return self.__operate__(other, operator.__ge__, inplace=False)

    def __lt__(self, other):
        return self.__operate__(other, operator.__lt__, inplace=False)

    def __le__(self, other):
        return self.__operate__(other, operator.__le__, inplace=False)

    def __eq__(self, X):
        '''If comparing to dense then element by element is necessary 
        '''
        return self.dense == X
        #inds = np.indices(X.shape)
        #for ind in inds:
        #    if X[ind] != self.__getitem__(ind) return False
        # TODO if X is sparse then compare defaults and check indices

    def sum(self):
        ''' Sum of elements. 
        >>> S = sparsearray((3,3), default=3)
        >>> S[1,1] = 10
        >>> S.sum()
        34.0
        '''
        #>>> sum(S) just hangs for some reason

        s = self._default * np.array(self.shape).prod()
        for ind in self._data:
            s += (self._data[ind] - self._default)
        return s

    # sparse specific functions not relevant to dense arrays
    def __len__(self):
        ''' Returns the number of non default elements.
        >>> S = sparsearray((3, 3), default=3)
        >>> len(S)
        0
        >>> S[1,1] = 10
        >>> len(S)
        1
        '''
        return len(self._data)

    # Non numpy array functions which are only applicable to sparse arrays
    def dense(self):
        """ Convert to dense NumPy array. 
        >>> S = sparsearray((3, 3), dtype=np.int)
        >>> S[1, 1] = 10
        >>> S.dense()
        array([[ 0,  0,  0],
               [ 0, 10,  0],
               [ 0,  0,  0]])
        """
        out = np.empty(self.shape, self.dtype)
        out[:] = self._default 
        for ind in self._data:
            out[ind] = self._data[ind]
        return out

    def nondefault(self):
        ''' Return all the non default elements as a list of index tuples and
        their corresponding values.
        >>> S = sparsearray((3,3))
        >>> S[1,1] = 10
        >>> S[1,2] = 9 
        >>> inds, vals = S.nondefault()
        >>> inds
        [(1, 2), (1, 1)]
        >>> vals
        [9.0, 10.0]
        >>> S.count_nondefault()
        2
        '''
        K = self._data.keys()
        V = self._data.values()
        return K, V

    def count_nondefault(self):
        return len(self._data)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
    #doctest.testmod(raise_on_error=True)
    #doctest.testmod(verbose=True)

#
#    #A[1,1] = 0 # TODO if set back to the default key should be deleted namely memory consumption should not grow.
#
#    #print 'remove an element...'
#    #del(A[2,2])
