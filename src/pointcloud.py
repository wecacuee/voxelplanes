import numpy as np

# This three function are copied from 
# /jcr/active/mrol_launchpad/mrol_mapping/occupiedlist.py
def _4_column_int16(inds):
    '''converts N x 3 array of voxel ints to N x 4 array of int16 with last
    column 0'''
    X_int16 = np.empty((inds.shape[0], 4), dtype=np.int16)
    X_int16[:, :3] = inds
    # 4th column needs to be zeros
    X_int16[:, 3] = 0
    #X_int16.dtype = np.int64
    #X_int16.shape = -1
    return X_int16

def _unpack(ids):
    '''converts an int64 array of voxel ids to an nx4 array of np.int16. 
    Near instantaneous in place operation'''
    ids.dtype = np.int16
    ids.shape = -1, 4

def _pack(inds):
    '''converts in place an integer array of Nx4 int16 voxels to an array of
    ids.  the last column are zeros.'''
    assert inds.dtype == np.int16, 'inds needs to be np.int16'
    assert inds.shape[1] == 4 
    assert not np.isfortran(inds), 'Incorrect array memory layout'

    inds.dtype = np.int64
    inds.shape = -1
# end of copy from occupiedlist.py

def boxfilter(xyzs, mins, maxs): # TODO there is a similar function in occupiedlist.py
    ''' Return points inside box from mins to maxs '''
    in_bounds = np.all((xyzs[:,:3] >= mins) & (xyzs[:,:3] < maxs), axis=1)
    return xyzs[in_bounds]

def voxels(xyzs, res):
    ''' converts n by 3 xyzs to n x 3 voxel inds '''
    return np.int32(xyzs/res)

def np_unique(ar, return_index=True):
    """
    numpy version 1.6.1 has a bug which was fixed in 1.6.2
    https://github.com/numpy/numpy/pull/237

    Bug: In case of repeated elements, np.unique() do not results in
    consistent results.
    >>> v=np.array([0,0,0,0,0,1,1,1,1,1,1,2,2,2,2,2,2])
    >>> w=np.array([0,0,0,0,0,1,1,1,1,1,1,2,2,2,2])
    >>> uv, partv = np_unique(v)
    >>> uw, partw = np_unique(w)
    >>> np.all(uv == uw)
    True
    >>> np.all(partv == partw)
    True

    By the time we upgrade to 1.6.2, we can use this copied version of np_unique
    """
    assert return_index == True
    perm = ar.argsort(kind='mergesort')
    aux = ar[perm]
    flag = np.concatenate(([True], aux[1:] != aux[:-1]))
    return aux[flag], perm[flag]

def group_by_voxel(xyzs, abcs):
    ''' Groups an Nx3 array of points so points within same group are in the same voxel '''
    # pack inds to create single integer
    abcs = _4_column_int16(abcs)
    _pack(abcs)
    # sort by integer and group
    sort_inds = np.argsort(abcs)
    xyzs = xyzs[sort_inds]
    abcs = abcs[sort_inds]

    uniq, partitions = np_unique(abcs, return_index=True)

    _unpack(uniq)
    return uniq[:,:3], partitions, xyzs

def grouped_iterator(xyzs, abcs):
    uniq, P, grouped = group_by_voxel(xyzs, abcs) 
    for i in range(len(P)-1):
        yield uniq[i], grouped[P[i]:P[i+1]]
    # last voxel is last partition to the end
    yield uniq[-1], grouped[P[-1]:] 

if __name__ == '__main__':
    np.random.seed(5)
    res = 0.4
    N = 50
    xyzs = np.random.random((N,3))
    #import data
    #xyzs = data.get_test_points()
    abcs = voxels(xyzs, res)
    #uniq, P, grouped = group_by_voxel(xyzs, abcs) 
    count = 0
    for ind, voxel_xyzs in grouped_iterator(xyzs, abcs):
        count += len(voxel_xyzs)
        V = voxels(voxel_xyzs, res)
        # ensure all points are in the same voxel
        assert np.all(V == V[0]), 'Incorrect grouping'
        #print ind
        #print voxel_xyzs
        #print
    assert count == len(xyzs), 'Different total number of points after grouping'
    import doctest
    doctest.testmod()
