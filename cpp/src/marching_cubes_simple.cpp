/*
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2010, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage, Inc. nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef SR_MARCHING_CUBES_HOPPE_H_
#define SR_MARCHING_CUBES_HOPPE_H_

#include <sr/marching_cubes_simple.h>
#include <pcl/common/common.h>
#include <pcl/common/vector_average.h>
#include <pcl/Vertices.h>
#include <pcl/kdtree/kdtree_flann.h>


#include <pcl/impl/instantiate.hpp>
#include <pcl/point_types.h>
#include <pcl/surface/impl/marching_cubes.hpp>
// getting fancy
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/count.hpp>
#include <algorithm>
namespace bacc = boost::accumulators;
namespace tag = bacc::tag;

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointNT>
sr::MarchingCubesSimple<PointNT>::MarchingCubesSimple ()
  : MarchingCubes<PointNT> ()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointNT>
sr::MarchingCubesSimple<PointNT>::~MarchingCubesSimple ()
{
}


//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointNT> void
sr::MarchingCubesSimple<PointNT>::voxelizeData ()
{
  const unsigned int width =  input_->width;
  const unsigned int height = input_->height;
  int npts = width * height;

  iso_level_ = 3.f/npts;

  float binsize_x = (max_x_ - min_x_) / res_x_;
  float binsize_y = (max_y_ - min_y_) / res_y_;
  float binsize_z = (max_z_ - min_z_) / res_z_;
  grid_.resize(res_x_ * res_y_ * res_z_);
  for (int i = 0; i < npts; i++) {
      PointNT pt = input_->points[i];
      if (min_x_ <= pt.x && pt.x < max_x_
          && min_y_ <= pt.y && pt.y < max_y_
          && min_z_ <= pt.z && pt.z < max_z_)
	{
          int x = static_cast<int>((pt.x - min_x_) / binsize_x);
          int y = static_cast<int>((pt.y - min_y_) / binsize_x);
          int z = static_cast<int>((pt.z - min_z_) / binsize_z);
          grid_[x * res_y_ * res_z_ + y * res_z_ + z] += 1.f/npts;
      }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////
template <typename PointNT> void
sr::MarchingCubesSimple<PointNT>::performReconstruction (pcl::PolygonMesh &output)
{
  if (!(iso_level_ >= 0 && iso_level_ < 1))
  {
    PCL_ERROR ("[pcl::%s::performReconstruction] Invalid iso level %f! Please use a number between 0 and 1.\n", getClassName ().c_str (), iso_level_);
    output.cloud.width = output.cloud.height = 0;
    output.cloud.data.clear ();
    output.polygons.clear ();
    return;
  }

  // Create grid
  grid_ = std::vector<float> (res_x_*res_y_*res_z_, 0.0f);

  // Populate tree
  tree_->setInputCloud (input_);

  getBoundingBox ();

  // Transform the point cloud into a voxel grid
  // This needs to be implemented in a child class
  voxelizeData ();



  // Run the actual marching cubes algorithm, store it into a point cloud,
  // and copy the point cloud + connectivity into output
  pcl::PointCloud<PointNT> cloud;

  for (int x = 1; x < res_x_-1; ++x)
    for (int y = 1; y < res_y_-1; ++y)
      for (int z = 1; z < res_z_-1; ++z)
      {
        Eigen::Vector3i index_3d (x, y, z);
        std::vector<float> leaf_node;
        getNeighborList1D (leaf_node, index_3d);
        createSurface (leaf_node, index_3d, cloud);
      }
  pcl::toROSMsg (cloud, output.cloud);

  output.polygons.resize (cloud.size () / 3);
  for (size_t i = 0; i < output.polygons.size (); ++i)
  {
    pcl::Vertices v;
    v.vertices.resize (3);
    for (int j = 0; j < 3; ++j)
      v.vertices[j] = static_cast<int> (i) * 3 + j;
    output.polygons[i] = v;
  }
}

#define PCL_INSTANTIATE_MarchingCubesSimple(T) template class PCL_EXPORTS sr::MarchingCubesSimple<T>;

// Instantiations of specific point types
PCL_INSTANTIATE(MarchingCubesSimple, (pcl::PointXYZ))
#endif    // SR_MARCHING_CUBES_HOPPE_H_

