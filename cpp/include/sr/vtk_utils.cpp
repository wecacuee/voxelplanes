#ifndef VTK_UTILS_H_
#define VTK_UTILS_H_

#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkSmartPointer.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>

namespace sr {
  void readPoints(vtkPoints* points, vtkCellArray *vertices, char* filename);

  void readPoints(vtkPoints* points, vtkCellArray *vertices, char* filename)
  {

    // Create points on a sphere
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

    if (pcl::io::loadPCDFile<pcl::PointXYZ>(filename, *cloud) == -1)
      {
	PCL_ERROR ("Couldn't read file %s \n", filename);
	return;
      }

    int npts = cloud->width * cloud->height;
    for (int i = 0; i < npts; i++) {
	pcl::PointXYZ pt = cloud->points[i];
	vtkIdType pid[1];
	pid[0] = points->InsertNextPoint(pt.x, pt.y, pt.z);
	vertices->InsertNextCell(1, pid);
    }

    return;
  }
}
#endif // VTK_UTILS_H_
