
#ifndef PCL_SURFACE_MARCHING_CUBES_HOPPE_H_
#define PCL_SURFACE_MARCHING_CUBES_HOPPE_H_

#include <pcl/surface/marching_cubes.h>

namespace sr
{
   /** \brief The marching cubes surface reconstruction algorithm, using a signed distance function based on the distance
     * from tangent planes, proposed by Hoppe et. al. in:
     * Hoppe H., DeRose T., Duchamp T., MC-Donald J., Stuetzle W., "Surface reconstruction from unorganized points",
     * SIGGRAPH '92
     * \author Alexandru E. Ichim
     * \ingroup surface
     */
  using namespace pcl;
  template <typename PointNT>
  class MarchingCubesSimple : public MarchingCubes<PointNT>
  {
    public:
      typedef boost::shared_ptr<MarchingCubesSimple<PointNT> > Ptr;
      typedef boost::shared_ptr<const MarchingCubesSimple<PointNT> > ConstPtr;

      using SurfaceReconstruction<PointNT>::input_;
      using SurfaceReconstruction<PointNT>::tree_;
      using MarchingCubes<PointNT>::grid_;
      using MarchingCubes<PointNT>::res_x_;
      using MarchingCubes<PointNT>::res_y_;
      using MarchingCubes<PointNT>::res_z_;
      using MarchingCubes<PointNT>::min_p_;
      using MarchingCubes<PointNT>::max_p_;
      using MarchingCubes<PointNT>::iso_level_;
      using MarchingCubes<PointNT>::getClassName;
      using MarchingCubes<PointNT>::getBoundingBox;
      using MarchingCubes<PointNT>::getNeighborList1D;

      typedef typename pcl::PointCloud<PointNT>::Ptr PointCloudPtr;

      typedef typename pcl::KdTree<PointNT> KdTree;
      typedef typename pcl::KdTree<PointNT>::Ptr KdTreePtr;

      /** \brief The grid minimums */
      float min_x_, min_y_, min_z_;

      /** \brief The grid maximums */
      float max_x_, max_y_, max_z_;

      /** \brief Constructor. */
      MarchingCubesSimple ();

      /** \brief Destructor. */
      ~MarchingCubesSimple ();

      /** \brief Convert the point cloud into voxel data. */
      void
      voxelizeData ();


      void
      performReconstruction (pcl::PolygonMesh &output);


    public:
      EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

      /**
       * \brief Set the minimum limit of the grid
       * \param[in] min_x the minimum along x-axis
       * \param[in] min_y the minimum along y-axis
       * \param[in] min_z the minimum along z-axis
       */
      inline void
      setGridMin(float min_x, float min_y, float min_z) {
          min_x_ = min_x;
          min_y_ = min_y;
          min_z_ = min_z;
      }

      /**
       * \brief Get the minimum limit of the grid
       * \param[in] min_x the minimum along x-axis
       * \param[in] min_y the minimum along y-axis
       * \param[in] min_z the minimum along z-axis
       */
      inline void
      getGridMin(float &min_x, float &min_y, float &min_z) {
          min_x = min_x_;
          min_y = min_y_;
          min_z = min_z_;
      }

      /**
       * \brief Get the maximum limit of the grid
       * \param[in] max_x the maximum along x-axis
       * \param[in] max_y the maximum along y-axis
       * \param[in] max_z the maximum along z-axis
       */
      inline void
      getGridMax(float &max_x, float &max_y, float &max_z) {
          max_x = max_x_;
          max_y = max_y_;
          max_z = max_z_;
      }

      /**
       * \brief Set the maximum limit of the grid
       * \param[in] max_x the maximum along x-axis
       * \param[in] max_y the maximum along y-axis
       * \param[in] max_z the maximum along z-axis
       */
      inline void
      setGridMax(float max_x, float max_y, float max_z) {
          max_x_ = max_x;
          max_y_ = max_y;
          max_z_ = max_z;
      }
  };
}

#endif  // PCL_SURFACE_MARCHING_CUBES_HOPPE_H_
