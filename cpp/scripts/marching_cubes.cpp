#include <boost/program_options.hpp>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/io/vtk_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <sr/marching_cubes_simple.h>
#include <pcl/surface/marching_cubes_hoppe.h>
#include <pcl/surface/marching_cubes_rbf.h>

namespace pio = pcl::io;
namespace po = boost::program_options;

/* ---[ */
int
  main (int argc, char** argv)
{
  // http://www.boost.org/doc/libs/1_53_0/doc/html/program_options/tutorial.html
  po::options_description desc("Usage: marching_cubes [--voxel_size 0.05] <filename>");
  desc.add_options()
    ("help", "produce help message")
    ("voxel_size", po::value<float>()->default_value(0.05), "set voxel size")
    ("input", po::value< std::vector<std::string> >(), "input file")
    ("output", po::value< std::vector<std::string> >(), "output file")
    ;
  po::positional_options_description pdesc;
  pdesc.add("input", 1).add("output", 1);

  po::variables_map vm;
  po::store(po::command_line_parser(argc, argv).
      options(desc).positional(pdesc).run(), vm);
  po::notify(vm);

  if (vm.count("help")) {
      std::cout << desc << std::endl;
      return 1;
  }

  if (!vm.count("input") || !vm.count("output")) {
      std::cout << "Needs input and output file" << std::endl;
      std::cout << desc << std::endl;
  }
  std::string input_file = vm["input"].as< std::vector<std::string> >()[0];
  std::string output_file =vm["output"].as< std::vector<std::string> >()[0];


  float voxel_size = 0.05;
  if (vm.count("voxel_size")) {
      voxel_size = vm["voxel_size"].as<float>();
  } 

  // Input
  //std::assert(argc > 2);
  const char* file_name = input_file.c_str();
  // Load input file into a PointCloud<T> with an appropriate type
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  pcl::io::loadPCDFile (file_name, *cloud);


  // Create search tree*
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree2 (new pcl::search::KdTree<pcl::PointXYZ>);
  tree2->setInputCloud (cloud);

  // Marching cubes meshification
  sr::MarchingCubesSimple<pcl::PointXYZ> mc;

  int max = 5;
  int min = -5;
  mc.setGridMin(min, min, min);
  mc.setGridMax(max, max, max);
  int res = static_cast<int>(ceil((max - min)/voxel_size));
  mc.setGridResolution(res, res, res);
 //mc.voxelizeData();

  // PCLBase
  mc.setInputCloud(cloud);
  // PCLSurfaceBase
  mc.setSearchMethod(tree2);

  pcl::PolygonMesh triangles;
  // pcl::SurfaceReconstruction
  mc.reconstruct(triangles);

  pio::saveVTKFile (output_file.c_str(), triangles);

  // Finish
  return (0);
}
/* ]--- */
