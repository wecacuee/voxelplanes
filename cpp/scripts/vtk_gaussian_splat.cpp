#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkContourFilter.h>
#include <vtkGaussianSplatter.h>
#include <vtkSphereSource.h>
 
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkInteractorStyleSwitch.h>

#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
 
int main(int argc, char ** argv)
{
  // Create points on a sphere
  pcl::PCDReader reader;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

  reader.read (argv[1], *cloud);
 
  int npts = cloud->width * cloud->height;
  vtkSmartPointer<vtkPoints> vpoints = vtkSmartPointer<vtkPoints>::New();
  vpoints->SetNumberOfPoints(npts);
  for (int i = 0; i < npts; i++) {
      pcl::PointXYZ pt = cloud->points[i];
      vpoints->InsertPoint(i, pt.x, pt.y, pt.z);
  }

  std::cout << vpoints->GetNumberOfPoints() << " points added" << std::endl;

  vtkSmartPointer<vtkPolyData> polydata = 
    vtkSmartPointer<vtkPolyData>::New();

  polydata->SetPoints(vpoints);

  // Create cell array
  vtkSmartPointer<vtkCellArray> verts =
    vtkSmartPointer<vtkCellArray>::New();
  for (int i = 0; i < npts; i++) {
      verts->InsertNextCell(VTK_VERTEX);
      verts->InsertCellPoint(i);
  }
  polydata->SetVerts(verts);
 
  vtkSmartPointer<vtkGaussianSplatter> splatter = 
    vtkSmartPointer<vtkGaussianSplatter>::New();
#if VTK_MAJOR_VERSION <= 5
  splatter->SetInput(polydata);
#else
  splatter->SetInputData(polydata);
#endif
  splatter->SetSampleDimensions(200,200,200);
  splatter->SetRadius(0.5);
  splatter->ScalarWarpingOff();
  splatter->SetModelBounds(-5., 5., -5., 5., -5., 5.);
 
  vtkSmartPointer<vtkContourFilter> surface = 
    vtkSmartPointer<vtkContourFilter>::New();
  surface->SetInputConnection(splatter->GetOutputPort());
  surface->SetValue(0,0.01);
 
  // Create a mapper and actor
  vtkSmartPointer<vtkPolyDataMapper> mapper = 
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInput(polydata);
 
  vtkSmartPointer<vtkActor> actor = 
    vtkSmartPointer<vtkActor>::New();
  actor->SetMapper(mapper);
 
  // Visualize
  vtkSmartPointer<vtkRenderer> renderer = 
    vtkSmartPointer<vtkRenderer>::New();
  vtkSmartPointer<vtkRenderWindow> renderWindow = 
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);
  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor = 
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);
  vtkSmartPointer<vtkInteractorStyleSwitch> interactorStyle = 
    vtkSmartPointer<vtkInteractorStyleSwitch>::New();
  renderWindowInteractor->SetInteractorStyle(interactorStyle);
 
  renderer->AddActor(actor);
  renderer->SetBackground(1,1,1); // Background color white
 
  renderWindow->Render();
  renderWindowInteractor->Start();
 
  return EXIT_SUCCESS;
 
}
