#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkImplicitModeller.h>
#include <vtkVoxelModeller.h>
#include <vtkMarchingCubes.h>
#include <vtkPolyDataWriter.h>
#include <vtkDataArray.h>

#include <cassert>
#include <cstdlib>
#include <cmath>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <sr/vtk_utils.cpp>

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using boost::filesystem::path;
namespace po = boost::program_options;

int
main(int argc, char** argv) {

    // http://www.boost.org/doc/libs/1_53_0/doc/html/program_options/tutorial.html
    po::options_description desc("Usage: vtk_marching_cubes [--voxel_size 0.05] <filename>");
    desc.add_options()
    ("help", "produce help message")
    ("voxel_size", po::value<float>()->default_value(0.05), "set voxel size")
    ("input", po::value< std::vector<std::string> >(), "input file")
    ("output", po::value< std::vector<std::string> >(), "output file")
    ;
    po::positional_options_description pdesc;
    pdesc.add("input", 1).add("output", 1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).
	options(desc).positional(pdesc).run(), vm);
    po::notify(vm);

    if (vm.count("help")) {
	std::cout << desc << std::endl;
	return 1;
    }

    if (!vm.count("input") || !vm.count("output")) {
	std::cout << "Needs input and output file" << std::endl;
	std::cout << desc << std::endl;
    }
    std::string input_file = vm["input"].as< std::vector<std::string> >()[0];
    std::string output_file =vm["output"].as< std::vector<std::string> >()[0];

    float voxel_size = 0.05;
    if (vm.count("voxel_size")) {
	voxel_size = vm["voxel_size"].as<float>();
    } 

    // Read some points
    vtkPoints *points = vtkPoints::New();
    vtkCellArray *vertices = vtkCellArray::New();
    sr::readPoints(points, vertices,
        const_cast<char *>(input_file.c_str()));

    //for (vtkIdType i = 0; i < points->GetNumberOfPoints(); i++) {
    //    double pt[3];
    //    points->GetPoint(i, pt);
    //    cout << " [" << pt[0] << ", " << pt[1] << ", " << pt[2] << "]" << endl;
    //}
    vtkPolyData *point_cloud = vtkPolyData::New();
    point_cloud->SetPoints(points);
    point_cloud->SetVerts(vertices);

    double min = -5.0;
    double max = 5.0;
    // 10/200 = 0.05 m  = 5cm, we should compute distance at like  ~10cm
    int nsamples = static_cast<int>(ceil((max - min) / voxel_size));

#define use_distance_function false


#if use_distance_function
    vtkSmartPointer<vtkImplicitModeller> voxelModeller
      = vtkSmartPointer<vtkImplicitModeller>::New();
    voxelModeller->SetOutputScalarTypeToFloat();
#else
    vtkSmartPointer<vtkVoxelModeller> voxelModeller = vtkSmartPointer<vtkVoxelModeller>::New();
    voxelModeller->SetScalarTypeToFloat();
#endif
    voxelModeller->SetModelBounds(min, max, min, max, min, max);
    voxelModeller->SetSampleDimensions(nsamples, nsamples, nsamples);
    voxelModeller->SetMaximumDistance(2*voxel_size*10 / (sqrt(3) * (max - min)));
    voxelModeller->SetInput(point_cloud);
    voxelModeller->Update();


    vtkSmartPointer<vtkMarchingCubes> marchingCubes = vtkSmartPointer<vtkMarchingCubes>::New();
    marchingCubes->SetInputConnection(voxelModeller->GetOutputPort());
    marchingCubes->ComputeNormalsOff();
    marchingCubes->SetValue(0, voxel_size);
    marchingCubes->Update();
    
    vtkSmartPointer<vtkPolyDataWriter> polyWriter = vtkSmartPointer<vtkPolyDataWriter>::New();
    polyWriter->SetFileName(output_file.c_str());
    polyWriter->SetInputConnection(marchingCubes->GetOutputPort());
    polyWriter->Update();
    polyWriter->Write();
}
