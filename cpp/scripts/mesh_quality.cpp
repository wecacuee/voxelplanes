
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkImplicitPolyData.h>
#include <vtkPolyDataReader.h>
#include <vtkDataArray.h>

#include <sr/vtk_utils.cpp>
#include <cassert>

#include <boost/filesystem.hpp>

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using boost::filesystem::path;

static float
point_to_mesh_distance(vtkPolyData* points_polydata, vtkPolyData* mesh) {


    vtkSmartPointer<vtkImplicitPolyData> imp = vtkImplicitPolyData::New();
    imp->SetInput( mesh );

    int numPts = points_polydata->GetNumberOfPoints();
    double total_distance = 0;
    for (vtkIdType ptId = 0; ptId < numPts; ptId++) {
      double pt[3];
      points_polydata->GetPoint( ptId, pt );
      double val = imp->EvaluateFunction( pt );
      //cerr << "Distance of pid:" << ptId << " [" << pt[0] << ", " << pt[1] << ", " << pt[2] << "]from cell:" << val << endl;
      total_distance += fabs(val);
    }
    return total_distance;
}

int
main(int argc, char** argv) {
    if (argc < 3) {
        cout << "Usage: " << argv[0] << " <points.pcd> <mesh.vtk>" << endl;
    }
    path ext = path(argv[1]).extension();
    if (ext != path(".pcd"))
      cout << "Expecting file type .pcd. Got :" << ext << endl;
    ext = path(argv[2]).extension();
    if (ext != path(".vtk"))
      cout << "Expecting file type .vtk. Got :" << ext << endl;

    // Read some points
    vtkPoints *points = vtkPoints::New();
    vtkCellArray *vertices = vtkCellArray::New();
    sr::readPoints(points, vertices, argv[1]);
    //for (vtkIdType i = 0; i < points->GetNumberOfPoints(); i++) {
    //    double pt[3];
    //    points->GetPoint(i, pt);
    //    cout << " [" << pt[0] << ", " << pt[1] << ", " << pt[2] << "]" << endl;
    //}
    vtkPolyData *point_cloud = vtkPolyData::New();
    point_cloud->SetPoints(points);
    point_cloud->SetVerts(vertices);
    // cout << "Points in point cloud:" << point_cloud->GetNumberOfVerts() << endl;
    
    vtkPolyDataReader* pdr = vtkPolyDataReader::New();
    pdr->SetFileName(argv[2]);
    pdr->Update();
    vtkPolyData *mesh = pdr->GetOutput();
    // cout << "Polys in mesh:" << mesh->GetNumberOfPolys() << endl;

    //cout << "Total distance from points to mesh:" << 
    cout << point_to_mesh_distance(point_cloud, mesh) << endl;
    return EXIT_SUCCESS;
}
