import numpy as np
import pcl_reader as pclr
import os
from voxel_planes import readpoints
import pointcloud

import argparse

def split(xyzs, test_sample_ratio):
    """
    Split the input point set into two sets with cvratio and 1 - cvratio
    """
    np.random.shuffle(xyzs)
    test_size = xyzs.shape[0] * test_sample_ratio
    test, training = xyzs[:test_size, :], xyzs[test_size:, :]
    assert test.shape[0] + training.shape[0] == xyzs.shape[0]
    return training, test

def clip_and_split(xyzs, test_sample_ratio):
    min = (-5, -5, -5)
    max = (5, 5, 5)
    xyzs = pointcloud.boxfilter(xyzs, min, max)
    xyzs = xyzs.astype(np.float32)
    training_xyzs, test_xyzs = split(xyzs, test_sample_ratio=test_sample_ratio)
    return training_xyzs, test_xyzs

def main(input_file, training_output, testoutput, test_sample_ratio):
    xyzs = readpoints(input_file)
    training_xyzs, test_xyzs = clip_and_split(xyzs)
    pclr.writepcd(open(training_output, 'w'), training_xyzs)
    pclr.writepcd(open(testoutput, 'w'), test_xyzs)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("inputfile", nargs="?", help="input file path")
    parser.add_argument("meshification_out", nargs="?", help="mesh out")
    parser.add_argument("test_out", nargs="?", help="test out")
    parser.add_argument("--mesh-ratio", help="meshification sample ratio",
                       default=0.9, type=float)
    args = parser.parse_args()
    main(parser.input_file, 
         parser.meshification_out,
         parser.test_out,
         1 - parser.mesh-ratio)
