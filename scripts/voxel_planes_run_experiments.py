from voxel_planes import readpoints
import pcl_reader as pclr
import split_for_cross_validation as split
import pointcloud
import StringIO
import re, os
import subprocess
import test_mesh_quality
from tvtk.api import tvtk 

FLOAT_SIZE = 4
INT_SIZE = 4

optimal_sample_ratio = 0.9
sample_ratios = [0.9, 0.85, 0.80, .75, .70, .65, .60, .55, .50, .40, .30, .20]
optimal_voxel_size = 0.05
voxel_sizes = [0.025, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.10, 0.15, 0.20, 0.30, 0.40]

class SampleRation2Npoints(object):
    def __init__(self):
        self.sr2npts = {}

    def add_entry(self,sr, npts):
        self.sr2npts[sr] = npts

    def npoints(self, sr):
        return self.sr2npts[sr]

sample_ratio_2_npoints = SampleRation2Npoints()

def temp_fname(input_file, temp, sr):
    base_file, ext = os.path.splitext(input_file)
    return (base_file + temp  % int(sr * 100))

def pcd_sampled_test_fname(input_file, sr):
    return temp_fname(input_file, "_%d_test.pcd", sr)

def pcd_sampled_filename(input_file, sr):
    return temp_fname(input_file, "_%d.pcd", sr)

def greedy_vtk_out_fname(input_file, sr):
    return temp_fname(input_file, "_%d_greedy.vtk", sr)

def greedy_log_fname(input_file, sr):
    return temp_fname(input_file, "_%d_greedy.log", sr)

def prepare_datasets(input_file):
    xyzs = readpoints(input_file)
    for sr in sample_ratios:
        meshification, test = split.clip_and_split(xyzs, test_sample_ratio=(1-sr))
        sample_ratio_2_npoints.add_entry(sr, len(meshification))
        pclr.writepcd( open(pcd_sampled_filename(input_file, sr), 'w'), meshification)
        pclr.writepcd( open(pcd_sampled_test_fname(input_file, sr), 'w'),
                      test)

def run_get_stdout(cmd):
    print("++++Executing " + " ".join(cmd) + "++++")
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    print "Got stdout", stdout
    print "Got stderr", stderr
    return stdout, stderr

def parse_running_time(stdout):
    lines = StringIO.StringIO(stdout)
    real_time_re = re.compile(r"real\s+(\d+\.\d+)$")
    sys_time_re = re.compile(r"sys\s+(\d+\.\d+)$")
    user_time_re = re.compile(r"user\s+(\d+\.\d+)$")
    #parse_time_re = re.compile(r"\s+(\d+\.\d+)system\s+\d:(\d+\.\d+)elapsed")
    for line in lines:
        m = real_time_re.findall(line)
        if m:
            real_time = float(m[0])
        m = sys_time_re.findall(line)
        if m:
            sys_time = float(m[0])
        m = user_time_re.findall(line)
        if m:
            user_time = float(m[0])
    return (sys_time + user_time), real_time

def get_mesh_representation_size(vtkout):
    polydatareader = tvtk.PolyDataReader(file_name=vtkout)
    polydatareader.update()
    polydata = polydatareader.output
    return (polydata.number_of_points * 3 * FLOAT_SIZE +
            polydata.number_of_polys * 3 * INT_SIZE)

def run_greedy_projection(input_file):
    cmd = ["time", "-p", "cpp/build/bin/greedy_projection", "<input_file>",
           "<output_file>"]
    base_name, ext = os.path.splitext(input_file)
    results_file = base_name + "_greedy_results.txt"
    resultsf = open(results_file, 'w')
    for sr in sample_ratios:
        vtkout = greedy_vtk_out_fname(input_file, sr)
        pcd_in = pcd_sampled_filename(input_file, sr)
        cmd[3] = pcd_in
        cmd[4] = vtkout
        log_f = greedy_log_fname(input_file, sr)
        out, err = run_get_stdout(cmd)
        cput, realt = parse_running_time(err)
        fsize = get_mesh_representation_size(vtkout)
        npts = sample_ratio_2_npoints.npoints(sr)
        error = get_error_measure(input_file, vtkout, sr)
        resultsf.write("%s\n" % " ".join(map(str, (cput, realt, fsize, npts,
                                                   error, error / npts))))

def vox_out_fname(input_file, sr, vs):
    return temp_fname(input_file, "_%%d_%f_vox.vtk" % vs, sr)

def mcubes_out_fname(input_file, sr, vs):
    return temp_fname(input_file, "_%%d_%f_mcubes.vtk" % vs, sr)

def parse_occ_voxels(stdout):
    lines = StringIO.StringIO(stdout)
    occ_vox_re = re.compile(r"Number of bins: (\d+)")
    for line in lines:
        m = occ_vox_re.findall(line)
        if m:
            return int(m[0])
    raise RuntimeError("Unable to find occupied voxels")

def run_voxel_planes(input_file, sr, vs):
    vox_cmd = ["time", "-p", "python", "src/voxel_planes.py", "<input_file>", "--out",
           "<output_file>", "--novis", "--res", "<res>"]
    pcd_in = pcd_sampled_filename(input_file, sr)
    vox_cmd[4] = pcd_in
    vox_out = vox_out_fname(input_file, sr, vs)
    vox_cmd[6] = vox_out
    vox_cmd[9] = str(vs)
    stdout, err = run_get_stdout(vox_cmd)
    print "Vox Stdout", stdout
    occ_vox = parse_occ_voxels(stdout)
    error = get_error_measure(input_file, vox_out, sr)
    vox_cpu_time, vox_real_time = parse_running_time(err)
    return occ_vox, vox_cpu_time, vox_real_time, error

def get_error_measure(input_file, output_file, sr):
    distance = test_mesh_quality.get_total_distance(
        pcd_sampled_test_fname(input_file, sr),
        output_file)
    return distance

def run_mcubes(input_file, sr, vs):
    mcubes_cmd = ["time", "-p", "cpp/build/bin/vtk_marching_cubes", "<input_file>",
                 "<output_file>", "--voxel_size", "<res>"]
    pcd_in = pcd_sampled_filename(input_file, sr)
    mcubes_cmd[3] = pcd_in
    mcubes_out = mcubes_out_fname(input_file, sr, vs)
    mcubes_cmd[4] = mcubes_out
    mcubes_cmd[6] = str(vs)
    stdout, err = run_get_stdout(mcubes_cmd)
    print "Mcubes Stdout", stdout
    mcubes_cpu_time, mcubes_real_time = parse_running_time(err)
    error = get_error_measure(input_file, mcubes_out, sr)
    return mcubes_cpu_time, mcubes_real_time, error

def run_voxel_planes_marching_cubes_cpu_npts(input_file):
    base_name, ext = os.path.splitext(input_file)
    vox_res_fname = base_name + "_vox_cpu_results.txt"
    vox_res_file = open(vox_res_fname, 'w')
    mcubes_res_fname = base_name + "_mcubes_cpu_results.txt"
    mcubes_res_file = open(mcubes_res_fname, 'w')
    vs = optimal_voxel_size
    for sr in sample_ratios:
        npts = sample_ratio_2_npoints.npoints(sr)

        occ_vox, vox_cpu_time, vox_real_time, vox_error = \
                run_voxel_planes(input_file, sr, vs)
        vox_res_file.write("%s\n" % " ".join(
            map(str, (vox_cpu_time, vox_real_time,
                      10 * FLOAT_SIZE * occ_vox, npts,
                      vox_error, vox_error / npts))))

        mcubes_cpu_time, mcubes_real_time, mcubes_error = run_mcubes(input_file, sr, vs)
        mcubes_res_file.write("%s\n" % " ".join(
            map(str, (mcubes_cpu_time, mcubes_real_time, 
                      FLOAT_SIZE * 1 * occ_vox,
                      npts, mcubes_error, mcubes_error / npts))))

def run_voxel_planes_marching_cubes(input_file):
    base_name, ext = os.path.splitext(input_file)
    vox_res_fname = base_name + "_vox_results.txt"
    vox_res_file = open(vox_res_fname, 'w')
    mcubes_res_fname = base_name + "_mcubes_results.txt"
    mcubes_res_file = open(mcubes_res_fname, 'w')
    sr = optimal_sample_ratio
    for vs in voxel_sizes:
        npts = sample_ratio_2_npoints.npoints(sr)

        occ_vox, vox_cpu_time, vox_real_time, vox_error = \
                run_voxel_planes(input_file, sr, vs)
        vox_res_file.write("%s\n" % " ".join(
            map(str, (vox_cpu_time, vox_real_time, 10 * 4 * occ_vox, npts,
                      vox_error, vox_error / npts))))

        mcubes_cpu_time, mcubes_real_time, mcubes_error = \
                run_mcubes(input_file, sr, vs)
        mcubes_res_file.write("%s\n" % " ".join(
            map(str, (mcubes_cpu_time, mcubes_real_time, 10 * 1 * occ_vox,
                      npts, mcubes_error, mcubes_error / npts))))

def poisson_vtk_out_fname(input_file, sr, vs):
    return temp_fname(input_file, "_%d_poisson.vtk", sr)

def run_poisson(input_file, sr, vs):
    cmd = ["time", "-p", "cpp/build/bin/poisson", "<input_file>", "<output_file>"]
    base_name, ext = os.path.splitext(input_file)
    pcd_in = pcd_sampled_filename(input_file, sr)
    poisson_out = poisson_vtk_out_fname(input_file, sr, vs)
    cmd[3] = pcd_in
    cmd[4] = poisson_out
    stdout, err = run_get_stdout(cmd)
    print "Poisson Stdout:\n", stdout
    poisson_cpu_time, poisson_real_time = parse_running_time(err)
    error = get_error_measure(input_file, poisson_out, sr)
    return poisson_cpu_time, poisson_real_time, error

def run_poisson_cpu_npts(input_file):
    base_name, ext = os.path.splitext(input_file)
    poisson_res_fname = base_name + "_poisson_results.txt"
    poisson_res_file = open(poisson_res_fname, 'w')
    vs = optimal_voxel_size
    for sr in sample_ratios:
        npts = sample_ratio_2_npoints.npoints(sr)
        poisson_cpu_time, poisson_real_time, poisson_error = \
                run_poisson(input_file, sr, vs)

        poisson_res_file.write("%s\n" % " ".join(
            map(str, (poisson_cpu_time, poisson_real_time, 0,
                      npts, poisson_error, poisson_error / npts))))

if  __name__ == '__main__':
    import sys
    prepare_datasets(sys.argv[1])
    run_greedy_projection(sys.argv[1])
    run_voxel_planes_marching_cubes(sys.argv[1])
    run_voxel_planes_marching_cubes_cpu_npts(sys.argv[1])
    #run_poisson_cpu_npts(sys.argv[1])
