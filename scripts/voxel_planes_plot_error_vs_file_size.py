# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Times']})
rc('text', usetex=True) 

import sys, os
if len(sys.argv) <= 1:
    print("Usage: scripts/voxel_planes_plot_error_vs_file_size.py results/20130313_r180_data/thermocolorlab_scan000.pcd")
    print("Usage: scripts/voxel_planes_plot_error_vs_file_size.py results/thermogauss/thermogauss.pcd")

input_file = sys.argv[1]
data_dir = os.path.dirname(input_file) + '/' #'results/thermogauss/'
root_name = os.path.splitext(os.path.basename(input_file))[0]#'thermogauss'
vox_r = np.loadtxt(data_dir + root_name + '_vox_results.txt')
mcubes_r = np.loadtxt(data_dir + root_name + '_mcubes_results.txt')
greedy_r = np.loadtxt(data_dir + root_name + '_greedy_results.txt')

plt.figure(figsize=(1.618*3 *1.1, 3*1.1))
plt.hold(True)
#vox_p, = plt.plot(vox_r[:, 2]/vox_r[0,2], vox_r[:, 5], 'b')
#mcubes_p, = plt.plot(mcubes_r[:, 2]/mcubes_r[0,2], mcubes_r[:, 5], 'g')
#greedy_p,= plt.plot(greedy_r[:, 2]/greedy_r[0,2], greedy_r[:, 5], 'r')
vox_p, = plt.plot(vox_r[:, 2] / vox_r[:, 3], vox_r[:, 5], 'bx-')
mcubes_p, = plt.plot(mcubes_r[:, 2] / mcubes_r[:, 3], mcubes_r[:, 5], 'gx-')
greedy_p,= plt.plot(greedy_r[:, 2] / greedy_r[:, 3], greedy_r[:, 5], 'rx-')
ax = plt.gca()
ax.set_xscale('log')
ax.set_yscale('log')
ax.set_xlabel("Representation size per point (bytes)")
ax.set_ylabel("Error: Distance per point (m)")
plt.legend((greedy_p, vox_p, mcubes_p),
           ('Greedy Projection', 'Voxel planes', 'Marching cubes'),
           loc='upper right',
           prop={'size':12})
plt.gcf().subplots_adjust(bottom=0.2)
plt.gcf().savefig('figures/voxel_planes_' + root_name + '_file_size_vs_error.pdf')
plt.show()

##
plt.figure(figsize=(1.618*3*1.1, 3*1.1))
vox_r = np.loadtxt(data_dir + root_name + '_vox_cpu_results.txt')
mcubes_r = np.loadtxt(data_dir + root_name + '_mcubes_cpu_results.txt')
greedy_r = np.loadtxt(data_dir + root_name + '_greedy_results.txt')
#poisson_r = np.loadtxt(data_dir + 'thermocolorlab_scan000_poisson_results.txt')
vox_p,    = plt.plot(vox_r[:, 3] / 1000, vox_r[:, 0], 'bx-')
mcubes_p, = plt.plot(mcubes_r[:, 3] / 1000, mcubes_r[:, 0], 'gx-')
greedy_p, = plt.plot(greedy_r[:, 3] / 1000, greedy_r[:, 0], 'rx-')
#poisson_p, = plt.plot(poisson_r[:, 3] / 1000, poisson_r[:, 0], 'r')
ax = plt.gca()
ax.set_xlabel("Number of points (in thousands)")
ax.set_ylabel("CPU time (seconds)")
plt.legend((greedy_p, vox_p, mcubes_p),#, poisson_p),
           ('Greedy Projection', 'Voxel planes', 'Marching cubes'),# 'Poisson'),
           loc=2, prop={'size':12})
plt.gcf().subplots_adjust(bottom=0.2)
plt.gcf().savefig('figures/voxel_planes_' + root_name + '_cpu_time_vs_npoints.pdf')
plt.show()
