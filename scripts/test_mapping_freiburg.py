#stdlib
import argparse
import os
import re
import itertools as it

#numpy
import scipy.misc as scimisc
import numpy as np

try:
    # comes with freiburg dataset
    # Get it from http://vision.in.tum.de/data/datasets/rgbd-dataset/tools
    import associate as bmtools_assc
    # transformations package 
    # http://www.lfd.uci.edu/~gohlke/code/transformations.py.html
    import transformations as tf
except ImportError, e:
    import sys
    from os.path import join as pjoin, dirname 
    # add edgevoxels/lib to the path
    sys.path.append(pjoin(
        dirname(dirname(__file__)), 'lib'))
    try:
        import associate as bmtools_assc
        import transformations as tf
    except ImportError, e:
        print("Please add edgevoxels/lib to the python path")
        raise

# modules from this package
import voxelmap
import utils
import visualiser
import voxel_planes
import data

### Constants 
res = 0.03
R = 4.0
MAX_DEPTH = 4.0 # cut off noisy points from point cloud

# visualise in a different process
visualiser.IPC_VIS = True

curdir = os.path.dirname(__file__)

def path(relpath):
    return os.path.join(curdir, relpath)

def slicegen(gen, n):
    """ Generator equivalent of gen[:n] """
    return it.imap(lambda x:x[1],
                   it.takewhile(lambda x:x[0] < n, enumerate(gen)))

def get_pointclouds(dir, depth, rgb, timestamp):
    for d, r, ts in it.izip(depth, rgb, timestamp):
        di = scimisc.imread(os.path.join(dir, d[0]))
        # mm -> m
        di = np.asarray(di, dtype=np.float32) / 10000.

        print("min-max (%f - %f)" % (np.min(di[~np.isnan(di) & (di != 0)]),
                                     np.max(di[~np.isnan(di)])))
        height, width = di.shape
        K = np.array([[525, 0, width/ 2.],
                      [0, 525, height/2.],
                      [0, 0, 1]])

        ri = scimisc.imread(os.path.join(dir, r[0]))

        xyzs = utils.depth2xyzs(di, K, color=ri, 
                               depth_filter=(~np.isnan(di)
                                             & (di != 0)
                                             & (di < MAX_DEPTH)))
        yield di, ri, xyzs, timestamp

_backup_pattern = re.compile("\.(\d+)$")
def isbackupfile(fname):
    match = _backup_pattern.search(fname)
    return match

def backup_file(fname):
    isbak = isbackupfile(fname)
    next = 1
    if isbak:
        # assume that fname exists
        next = int(isbak.group(1)) + 1
        nextfname = _backup_pattern.sub(".%d"%next, fname)
    else:
        nextfname = fname + ".%d"%next
    if os.path.exists(nextfname):
        backup_file(nextfname)
    os.rename(fname, nextfname)
    return True

class TestMapper:
    def __init__(self, dir):
        depth  = bmtools_assc.read_file_list(os.path.join(dir, "depth.txt"))
        rgb    = bmtools_assc.read_file_list(os.path.join(dir, "rgb.txt"  ))
        mapped = bmtools_assc.associate(depth, rgb, 0, 0.2)
        self.pointclouds = get_pointclouds(dir,
                                           (depth[m[0]] for m in mapped),
                                           (rgb[m[1]] for m in mapped),
                                           (m[0] for m in mapped)
                                          )

        voxelmap.VoxelcloudMap.registration = \
                voxelmap.Plane2PointICP(downsample_res_to_map_res=0)
        self.mapper = voxelmap.VoxelcloudMap((-R, -R, -R), (R, R, R),
                                             resolution=res)
        est_traj = os.path.join(dir, 'estimated_trajectory.txt')
        if os.path.exists(est_traj) and not backup_file(est_traj):
            raise RuntimeError("Unable to backup file:{0}".format(est_traj))
        self.est_traj_file = est_traj


    def _map_build(self, dir, scans):
        est_traj = open(self.est_traj_file, 'w')
        pointclouds = (self.pointclouds if scans == -1 else
                       slicegen(self.pointclouds, scans))
        for di, ri, pc, ts in pointclouds:
            print("Adding next point cloud")
            self.mapper.addpoints(pc)
            T = self.mapper.get_estimated_transform()
            est_traj.write(' '.join([str(ts),]
                              + [str(p) for p in T[:3, 3]]
                              + [str(p) for p in tf.quaternion_from_matrix(T)]
                                   ) + "\n")
            # prepare visualization and write to /dev/shm
            vertices, EV = voxel_planes.planes3(self.mapper)

def benchmark_test(dir, scans):
    tm = TestMapper(dir)
    vmap = tm._map_build(dir, scans)
    return vmap

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='''
        test with freiburg dataset http://vision.in.tum.de/data/datasets/rgbd-dataset

        Example usage from edgevoxels/src directory :

            python ../scripts/test_mapping_freiburg.py --dir ~/data/rgbd_dataset_freiburg1_360/
        ''')
    parser.add_argument('--dir', 
                        help='''directory with data set. It should have
                        the following files: depth.txt, rgb.txt,
                        groundtruth.txt, depth/, rgb/''')
    parser.add_argument('--scans', 
                        default=-1,
                        type=int,  
                        help='''Number of scans to read for testing''')
    args = parser.parse_args()
    if args.dir is None:
        args.dir = data.get_rgbd_freiburg_dataset("/tmp")
    benchmark_test(args.dir, args.scans)
