#!/usr/bin/python
"""
Test case for mesh_quality.cpp
"""
import numpy as np
from tvtk.api import tvtk
import pcl_reader
import subprocess

def points_polydata(xyzs):
    polyd = tvtk.PolyData()
    polyd.points = xyzs
    verts = np.arange(0, xyzs.shape[0], 1, 'l') # point ids
    verts.shape = (xyzs.shape[0], 1) # make it a column vector
    polyd.verts = verts
    return polyd

def delaunay2d(xyzs):
    polyd = points_polydata(xyzs)

    #mapper = tvtk.PolyDataMapper(input=polyd)
    #actor = tvtk.Actor(mapper=mapper)

    cleaner = tvtk.CleanPolyData(input=polyd)
    delaunay2D = tvtk.Delaunay2D(input_connection=cleaner.output_port, tolerance=0.1)
    return delaunay2D.output

def generate_test_data(npts=100, span_pts=10, span_wall=20, nwallpts=40,
                       dist=1, pf='/tmp/points.pcd', mf='/tmp/mesh.vtk'):
    pts = np.zeros((npts, 3), dtype=np.float32)
    pts[:, :2] = np.random.random((npts, 2)) * span_pts
    pcl_reader.writepcd(open(pf,'w'), pts)

    wall2 = np.ones((nwallpts, 3)) * dist
    wall2[:, :2] = np.random.random((nwallpts, 2)) * span_wall - 5
    delaunay_polyd = delaunay2d(wall2)
    pdw = tvtk.PolyDataWriter(input=delaunay_polyd,
                              file_name=mf)
    pdw.write()
    return pf, mf

def get_total_distance(points_file, mesh_file):
    cmd = ["cpp/build/bin/mesh_quality", points_file, mesh_file]
    print("+++++++Executing: %s" % " ".join(cmd))
    pi = subprocess.Popen(cmd,
                          stdout=subprocess.PIPE)
    stdout, _ = pi.communicate()
    return float(stdout.strip())

def visualize(points_file, mesh_file):
    subprocess.call(["pcd_viewer", "multiview=1", points_file, mesh_file])

if __name__ == '__main__':
    npts = 100
    pf, mf = generate_test_data(npts=npts)
    tot_dist = get_total_distance(pf, mf)
    if  np.abs(tot_dist - npts) > 0.01:
        print "Assertion failed: %f <=> %d" % (tot_dist, npts)
        visualize(pf, mf)


