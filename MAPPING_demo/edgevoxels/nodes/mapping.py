#!/usr/bin/python
import numpy as np
import time
import threading

# ros
import roslib;roslib.load_manifest('edgevoxels')
import rospy
from message_filters import Subscriber
from approxsync import ApproximateSynchronizer
from sensor_msgs.msg import Image
from nav_msgs.msg import Odometry
import tf
import tf.transformations as transf
import cv_bridge

# edgevoxels
from voxelmap import Plane2PointICP
from voxelcloud import Voxelcloud, VoxelCloudData, FlannCorrespondence
import devshmio
import utils
from utils import log

from ros_message_filters import TfMessageFilter

### Constants 
res = 0.05
R = 5.0

class OdomFromNavMsg(object):
    """Get transform from nav_msg.msg.Odometry"""
    def __init__(self, mapping_node):
        rospy.Subscriber("odom", Odometry, self.callback_odom)
        self.mapping_node = mapping_node
        self._last_odom = None

    def callback_odom(self, odom):
        # used by OdomFromNavMsg
        self._last_odom = odom

    def __call__(self, timestamp):
        odom = self._last_odom # ignoring timestamp
        if odom is None:
            return
        pos = odom.pose.pose.position
        trans = (pos.x, pos.y, pos.z)
        orientation = odom.pose.pose.orientation
        rotq = (orientation.x, orientation.y, orientation.z, orientation.w)
        T = transf.concatenate_matrices(transf.translation_matrix(trans),
                                        transf.quaternion_matrix(rotq))
        return T

class OdomFromTf(object):
    """Get transform from tf """
    def __init__(self, listener, odom_frame, target_frame, mapping_node):
        self.listener = listener
        self.lastT = transf.identity_matrix()
        self.odom_frame = odom_frame
        self.target_frame = target_frame
        self.mapping_node = mapping_node

    def __call__(self, timestamp):
        #lagged_timestamp = timestamp - rospy.Duration.from_sec(3)
        lagged_timestamp = timestamp
        odom_frame = self.odom_frame
        target_frame = self.target_frame
        try:
            self.listener.waitForTransform(odom_frame,
                                           target_frame,
                                           lagged_timestamp,
                                           rospy.Duration.from_sec(10))
            (trans,rotq) = self.listener.lookupTransform(odom_frame,
                                                         target_frame,
                                                         lagged_timestamp)
        except (tf.LookupException, tf.ConnectivityException,
                tf.ExtrapolationException), e:
            rospy.logerr(e)
            return
        except Exception, e:
            rospy.logerr("Generic exception")
            rospy.logerr(e)
            return

        # convert to transformation matrix
        T = transf.concatenate_matrices(transf.translation_matrix(trans),
                                        transf.quaternion_matrix(rotq))
        return T

# deprecated, should be deleted
class ThreadedVoxelcloud(Voxelcloud):
    """Adds points to voxelcloud in a different thread.  

    But you have to make VoxelcloudData type thread safe before using it. 
    Why is that?
    Because registration takes place in a different thread that accesses
    VoxelcloudData while Voxelcloud is busy modify it.
    You can use voxelcloud.ThreadsafeWrap for that.
    """
    def __init__(self, *args, **kwargs):
        Voxelcloud.__init__(self, *args, **kwargs)

        self.queue_lock = threading.Lock()
        self.queue_xyz_rgbs = None
        self.queue_T = None
        self.queue_addpoints_retval = None
        self.addpoints_thread = \
                threading.Thread(target=self.slow_addpoints_thread)
        self.addpoints_thread.start()

    def slow_addpoints_thread(self):
        # wait for first input
        while (not rospy.is_shutdown()) and (self.queue_T is None):
            rospy.sleep(1)
        while not rospy.is_shutdown():
            with self.queue_lock:
                xyz_rgbs = self.queue_xyz_rgbs
                T = self.queue_T
            retval = Voxelcloud.addpoints(self, xyz_rgbs, T)
            with self.queue_lock:
                self.queue_addpoints_retval = retval

    def addpoints(self, xyz_rgbs, T):
        with self.queue_lock:
            self.queue_xyz_rgbs = xyz_rgbs
            self.queue_T = T

        # wait for first output
        while (not rospy.is_shutdown()) and (self.queue_addpoints_retval is None):
            rospy.sleep(1)
        with self.queue_lock:
            return self.queue_addpoints_retval


class MappingWithICP(Voxelcloud):
    registration = Plane2PointICP()
    voxeltype = VoxelCloudData
    def __init__(self, *args, **kwargs):
        Voxelcloud.__init__(self, *args, **kwargs)
        self.odomDrift = transf.identity_matrix()
        self.last_transform = None

    def get_estimated_transform(self):
        return self.last_transform 

    def get_odom_drift(self):
        return self.odomDrift

    def addpoints(self, xyz_rgbs, odom_pose):
        xyzs = xyz_rgbs[:, :3]
        T = odom_pose.dot(self.odomDrift)
        if self.count_bins() > 0:
            T, err = self.registration.compute_transformation(self, xyzs, T)
            self.odomDrift = np.linalg.inv(odom_pose).dot(T)
        self.last_transform = T
        return Voxelcloud.addpoints(self, xyz_rgbs, T)

class MappingNode(object):
    voxelmaptype = MappingWithICP
    def __init__(self):
        # initalize voxel planes
        # Configure registration class
        # use downsample_res_to_map_res = 0 for no down sampling
        self.voxelmaptype.registration = \
                Plane2PointICP(downsample_res_to_map_res=0.2)
        self.vc = self.voxelmaptype((-R, -R, -R), (R, R, R), resolution=res)
        self.bridge = cv_bridge.CvBridge()
        self.odom_frame = rospy.get_param('~odom_frame_id')
        self.target_frame = rospy.get_param('~cam_frame_id')
        self.map_frame = rospy.get_param('~map_frame_id')

        self.tfbroadcaster = tf.TransformBroadcaster()

        self._processing_finish_time = None

    def callbackhandler(self, depthmsg, image, trans_rot):
        """
        handle incoming image and depth
        """
        # query pose from visual odometry
        trans, rotq = trans_rot
        T = transf.concatenate_matrices(transf.translation_matrix(trans),
                                        transf.quaternion_matrix(rotq))

        start = time.time()
        if self._processing_finish_time is not None:
            waitingtime = start - self._processing_finish_time
            print("Waiting time:", waitingtime)

        # convert images to opencv
        depth = self.bridge.imgmsg_to_cv(depthmsg,
                                         desired_encoding="passthrough")
        depth = np.asarray(depth, dtype=np.float32)
        # print "depth min-max (%f - %f)" % (np.min(depth[(depth != 0) &
        #                                                 ~np.isnan(depth)]),
        #                                    np.max(depth[~np.isnan(depth)]))

        image = self.bridge.imgmsg_to_cv(image, desired_encoding="rgb8")
        image = np.asarray(image)

        # opencv to xyz point cloud
        #K = np.asarray(camera_info.K)
        #K.shape = 3, 3
        height, width = depth.shape
        K = np.array([[525, 0, width/ 2.],
                      [0, 525, height/2.],
                      [0, 0, 1]])
        xyzs = utils.depth2xyzs(depth, K, color=image,
                                depth_filter=(~np.isnan(depth) & (depth != 0)
                                              & (depth < R)))
        if not len(xyzs):
            rospy.logwarn("No points to process")
            return

        # add xyzs to voxel planes
        if not self.vc.addpoints(xyzs, T):
            # no points added, nothing to do
            return

        Tdrift = self.vc.get_odom_drift()

        self.tfbroadcaster.sendTransform(
            Tdrift[:3, 3],
            transf.quaternion_from_matrix(Tdrift),
            depthmsg.header.stamp,
            self.odom_frame,
            self.map_frame
        )

        # prepare visualization and write to /dev/shm
        # vertices, EV = voxel_planes.planes3(self.vc)
        log("Updating voxelcloud.pickle")
        devshmio.dump2devshm(self.vc, "voxelcloud")
        self._processing_finish_time = time.time()
        print("Processing time:",  self._processing_finish_time - start)

def main():
    rospy.init_node('mapping')
    tss = ApproximateSynchronizer(0.1, # seconds
                                  [Subscriber("image_depth", Image),
                                   Subscriber("image", Image)],
                                  5) # queue size

    odom_frame = rospy.get_param('~odom_frame_id')
    target_frame = rospy.get_param('~cam_frame_id')
    tfmsg = TfMessageFilter(tss, odom_frame, target_frame, queue_size=1000)

    mnode = MappingNode()
    tfmsg.registerCallback(mnode.callbackhandler)
    rospy.spin()

if __name__ == '__main__':
    main()

