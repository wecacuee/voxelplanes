#! /bin/bash

# AER demo install script from a base install of Ubuntu 12.10

# TODO this is only partially automatic still needs careful inspection of
# the output and to be executed part by part

# update ubuntu
sudo apt-get update
sudo apt-get -y dist-upgrade
# reboot

# install useful packages
sudo apt-get -y install byobu vim

# Install ROS
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu precise main" > /etc/apt/sources.list.d/ros-latest.list'
wget http://packages.ros.org/ros.key -O - | sudo apt-key add -
sudo apt-get update
sudo apt-get -y install ros-groovy-ros-base ros-groovy-openni-launch ros-groovy-nav-msgs

# Install FOVIS library.
cd
wget https://fovis.googlecode.com/files/libfovis-1.0.0.tar.gz
tar -xzf libfovis-1.0.0.tar.gz
cd libfovis
mkdir build
cd build
cmake ..
make
sudo make install

# Install [FOVIS ROS wrapper](http://wiki.ros.org/fovis)
# install FOVIS library and ROS wrapper
sudo apt-get -y install subversion
cd
svn co https://fovis-pkg.googlecode.com/svn/trunk/
source /opt/ros/groovy/setup.bash
export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:~/trunk/fovis_lib
sudo rosdep init
rosdep update
rosmake fovis
rosmake fovis_lib

# Install edgevoxel repo
cd 
sudo apt-get -y install git
git clone https://jryde@bitbucket.org:/jryde/edgevoxels
sudo apt-get -y install python-scipy python-matplotlib python-numpy mayavi2

# check openni xtio pro is working with
# rostopic echo /camera/rgb/image_color

# Install OpenNI2 ROS wrappers via catkin because OpenNI do not work in virtual machine
cd catkin_ws
catkin_make
sudo ln -s libOpenNI2.so.0 libOpenNI2.so

#rosmake fovis and check bin directory in roscd fovis

export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:~/edgevoxels/ros

# Run aer demo

# In one terminal/screen
roslaunch openni_launch openni.launch
# In another terminal/screen
roslaunch edgevoxels voxel_planes.launch
