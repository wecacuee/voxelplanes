if [[ ! $ROS_ROOT ]]
then
	echo "source ~/wrk/ROS/setup.sh"
	source /opt/ros/groovy/setup.bash
fi
selfpath=$(readlink -m $BASH_SOURCE)
selfdir=$(dirname $selfpath)
# export CATKIN_ENV_HOOK_WORKSPACE=$(dirname $(dirname $ROS_ROOT))
# source $selfdir/catkin_ws/devel/setup.sh
echo "Adding $selfdir to package path"
export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:$selfdir
source ~/wrk/fovis/setup.sh
