Voxel planes creates a realtime mesh from a continuously updated pointcloud.

Authors: [Julian Ryde](http://www.cse.buffalo.edu/~jryde/)
[Vikas Dhiman](http://vikasdhiman.com)

Original paper: [pdf](http://www.cse.buffalo.edu/~jryde/publications/iros2013_voxel_planes.pdf)

# Running voxel planes mapping demo with Asus xtion

* [Install ROS](http://wiki.ros.org/groovy/Installation/Ubuntu) and dependencies

	For Ubuntu 12.10:

	    sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu quantal main" > /etc/apt/sources.list.d/ros-latest.list'
	    wget http://packages.ros.org/ros.key -O - | sudo apt-key add -
	    sudo apt-get update
	    sudo apt-get install ros-groovy-ros-base ros-groovy-openni-launch ros-groovy-nav-msgs
	
The following assume that you download everything to your home directory 

* Install FOVIS library.

    * Download [fovis](https://fovis.googlecode.com/files/libfovis-1.0.0.tar.gz)

	* Make and install
	
		    cd libfovis
		    mkdir build
		    cd build
		    cmake ..
		    make
		    sudo make install
		
	* execute in terminal or put in .bashrc
	
            export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib
		
* Install [FOVIS ROS wrapper](http://wiki.ros.org/fovis)

        svn co https://fovis-pkg.googlecode.com/svn/trunk/
        export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:~/fovis_lib
        sudo rosdep init
        rosdep update
        rosmake fovis
        rosmake fovis_lib

* Get edgevoxel repo

        git clone https://jryde@bitbucket.org:/jryde/edgevoxels

* Install packages for edgevoxels

        sudo apt-get install python-scipy python-matplotlib python-numpy mayavi2

* Add edgevoxels/ros/edgevoxels to the ROS package path

        export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:~/edgevoxels/ros

* Run voxel\_planes.launch

        roslaunch edgevoxels voxel_planes.launch

## Rerunning experiments

* Build cpp dependencies.

    sudo apt-get install libqhull-dev
    sudo add-apt-repository ppa:v-launchpad-jochen-sprickerhof-de/pcl
    sudo apt-get update
    sudo apt-get install libpcl-all
    cd cpp/; mkdir build
    cmake ..
    make

* Add src/ and lib/ to the PYTHONPATH
  
    export PYTHONPATH=$PYTHONPATH:src/:lib/

* Running experiments on thermocolorlab dataset. The following will save
experimental results in the data/ directory. *.vtk files can be visualized
with pcl_viewer
    
    python scripts/voxel_planes_run_experiments.py data/thermocolorlab_scan000.pcd

* The plots in the paper can be plotted by using (it uses the data saved by
above script in the data directory):
  
    python scripts/voxel_planes_plot_error_vs_file_size.py data/thermocolorlab_scan000.pcd


## Running mapping on the freiburg dataset.

* Run the mapping script on freiburg dataset. It will automatically download
the dataset.

    python scripts/test_mapping_freiburg.py
      
* The above script places the *.vtk file in /dev/shm directory. To visiualize
it run the following script:

    python src/vis_process.py
    
## Acknowledgements

This material is based upon work supported by the Fed-
eral Highway Administration through CUBRC’s Coopera-
tive Agreement No. DTFH61-07-H-00023 and NASA under
grants NNX12AQ69G and NNX12AM06G. Any opinions,
findings, conclusions or recommendations are those of the
authors and do not necessarily reflect the views of the FHWA
or NASA.
